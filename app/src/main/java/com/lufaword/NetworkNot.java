package com.lufaword;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.lufaword.Utils.AppStatus;

public class NetworkNot extends AppCompatActivity {
    private LinearLayout mainLay1;
    private ImageView Image;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_network);
        mainLay1 = (LinearLayout) findViewById(R.id.mainLay1);
        Image = (ImageView) findViewById(R.id.Image);

        Glide.with(this).load(R.drawable.nonetwork).into(Image);
        repeat();



    } //onCreate closer
    private void repeat() {
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppStatus.getInstance(NetworkNot.this).isOnline()) {
                    Intent intent=new Intent(NetworkNot.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    repeat();
                }
            }
        },4000);
    }
}
