package com.lufaword.ExtraScreens;

class Recent {
    private String keyword;
    Recent(String keyword) {
        this.keyword=keyword;
    }

    //getter and setter method

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
