package com.lufaword.ExtraScreens;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lufaword.Friends.FrndRec2;
import com.lufaword.Friends.SearchFrnd;
import com.lufaword.R;

import java.util.ArrayList;

class RecentRec extends RecyclerView.Adapter<RecentRec.Holder> {
    private Context mcontext;
    private ArrayList<Recent> recent;
    RecentRec(Context mcontext, ArrayList<Recent> recent) {
        this.mcontext=mcontext;
        this.recent=recent;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.recent_lay,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.nameofuser.setText(recent.get(position).getKeyword());

        // click on nameofuser
        holder.nameofuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mcontext, SearchFrnd.class);
                intent.putExtra("frnd_name",recent.get(position).getKeyword());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return recent.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView nameofuser;



        Holder(@NonNull View itemView) {
            super(itemView);
            nameofuser = (TextView) itemView.findViewById(R.id.nameofuser);
        }
    }
}
