package com.lufaword.ExtraScreens;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.Friends.FrndRec2;
import com.lufaword.Friends.Frnds;
import com.lufaword.Friends.SearchFrnd;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExtraSearch extends AppCompatActivity {
    private LinearLayout searchLay;
    private LinearLayout logo;
    private ImageView logoImage;
    private FrameLayout searchFrndFrameLay;
    private AutoCompleteTextView searchEditBox;
    private ImageView realSearchBtn;
    private TextView text;
    RequestQueue queue1;

    ArrayAdapter<String> adapter;
    ArrayList<String> arr=new ArrayList<>();
    private LinearLayout noRecentLay;
    private RecyclerView recentRec;
    ArrayList<Recent>recent;
    private TextView text2;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_lay);
        searchLay = (LinearLayout) findViewById(R.id.search_lay);
        logo = (LinearLayout) findViewById(R.id.logo);
        logoImage = (ImageView) findViewById(R.id.logo_image);
        searchFrndFrameLay = (FrameLayout) findViewById(R.id.search_frnd_frame_lay);
        searchEditBox = (AutoCompleteTextView) findViewById(R.id.search_edit_box);
        realSearchBtn = (ImageView) findViewById(R.id.real_search_btn);
        text = (TextView) findViewById(R.id.text);
        noRecentLay = (LinearLayout) findViewById(R.id.no_recent_lay);
        recentRec = (RecyclerView) findViewById(R.id.recent_rec);
        text2 = (TextView) findViewById(R.id.text2);

        // initilize array list
        recent=new ArrayList<>();
        recent.clear();

        new loadAllFriends().execute();
        new recent().execute();

/*        searchEditBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i== EditorInfo.IME_ACTION_DONE)
                {
                    String search_frnd=searchEditBox.getText().toString();
                    if(search_frnd.length()==0) {
                        Toast.makeText(getApplicationContext(), "Empty searchbox", Toast.LENGTH_SHORT).show();
                        searchEditBox.requestFocus();


                    }
                    else
                    {
                        Intent intent=new Intent(getApplicationContext(), SearchFrnd.class);
                        intent.putExtra("frnd_name",search_frnd);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                    return true;
                }
                return false;
            }
        });*/

        realSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String search_frnd=searchEditBox.getText().toString();
                if(search_frnd.length()==0) {
                    Toast.makeText(getApplicationContext(), "Empty searchbox", Toast.LENGTH_SHORT).show();
                    searchEditBox.requestFocus();

                }
                else
                {
                    Intent intent=new Intent(ExtraSearch.this, SearchFrnd.class);
                    intent.putExtra("frnd_name",search_frnd);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });

    } //onCreate closer
    @SuppressLint("StaticFieldLeak")
    private class loadAllFriends extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue1=new RequestQueue(cache1,network1);
            queue1.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.all_friends, new Response.Listener<String>() {
                @Override
                public void onResponse(String response2) {
                    parse2(response2);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));


                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue1.add(request);
            return null;
        }
    }

    private void parse2(String response2) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);

                    String name = jsonObject1.getString("name");

                    arr.add(name);
                    // setup auto complete view
                    adapter=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1, arr);
                    searchEditBox.setThreshold(1);//will start working from first character
                    searchEditBox.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                    searchEditBox.setTextColor(Color.RED);
                    //searchEditBox.setDropDownBackgroundResource(R.color.white);
                }
            }
            else{

                Toast.makeText(getApplicationContext(), "Network Issue", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class recent extends AsyncTask<String,String,String> {



        @Override
        protected String doInBackground(String... strings) {
            Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue1=new RequestQueue(cache1,network1);
            queue1.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.recent_search, new Response.Listener<String>() {
                @Override
                public void onResponse(String response2) {
                    parse3(response2);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));


                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue1.add(request);
            return null;
        }
    }

    private void parse3(String response2) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String keyword = jsonObject1.getString("keyword");

                    Recent recent1 = new Recent(keyword);
                    recent.add(recent1);

                    RecentRec recentRec1 = new RecentRec(getApplicationContext(), recent);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    recentRec.setLayoutManager(layoutManager);
                    recentRec.setHasFixedSize(true);
                    recentRec.setAdapter(recentRec1);
                    //suggestFRec.scrollToPosition(5); //use to focus the item with index
                    //rSuugested.notifyDataSetChanged();

                }
            }
            else
            {
                text2.setVisibility(View.GONE);
                recentRec.setVisibility(View.GONE);
                noRecentLay.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
