package com.lufaword.ExtraScreens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.lufaword.BuildConfig;
import com.lufaword.Campaign.Active;
import com.lufaword.Campaign.RActive;
import com.lufaword.Campaign.VideoPlayer;
import com.lufaword.Configuration.Config;
import com.lufaword.Payments.Paymain2;
import com.lufaword.R;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ActiveCamp extends AppCompatActivity {
    private LinearLayout searchLay;
    private ImageView goBaack;
    private ImageView imageView;
    private YouTubeThumbnailView youtubeImage;
    private ImageView playIcon;
    private TextView healine;
    private TextView desc;
    private ImageView share;
    private TextView shareTxt;
    private ImageView targetBackImage;
    private TextView helpingLine;
    private ContentLoadingProgressBar donateProgress;
    private TextView achieveText;
    private TextView dayLeft;
    private Button donateBtn;
    private TextView noFees;
    String camp_sr;
    RequestQueue queue1;
    String min,hr,day,msg_video,msg_image,msg_title,msg_desc,target_amount,rec_amount;
    private int w,minn;
    String sr;
    ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_camp);
        searchLay = (LinearLayout) findViewById(R.id.search_lay);
        goBaack = (ImageView) findViewById(R.id.goBaack);
        imageView = (ImageView) findViewById(R.id.imageView);
        youtubeImage = (YouTubeThumbnailView) findViewById(R.id.youtube_image);
        playIcon = (ImageView) findViewById(R.id.play_icon);
        healine = (TextView) findViewById(R.id.healine);
        desc = (TextView) findViewById(R.id.desc);
        share = (ImageView) findViewById(R.id.share);
        shareTxt = (TextView) findViewById(R.id.share_txt);
        targetBackImage = (ImageView) findViewById(R.id.target_back_image);
        helpingLine = (TextView) findViewById(R.id.helping_line);
        donateProgress = (ContentLoadingProgressBar) findViewById(R.id.donate_progress);
        achieveText = (TextView) findViewById(R.id.achieve_text);
        dayLeft = (TextView) findViewById(R.id.day_left);
        donateBtn = (Button) findViewById(R.id.donate_btn);
        noFees = (TextView) findViewById(R.id.no_fees);
        // initilize progress dialogue box
        pd=new ProgressDialog(this);
        pd.setTitle("Fetching Data");
        pd.setMessage("Please wait...");


        // receive intents
        Intent intent=getIntent();
        camp_sr=intent.getStringExtra("camp_sr");

        // load Active Campaign
        loadActiveCampaign("T","T",camp_sr);


        playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), VideoPlayer.class);
                intent.putExtra("video_id",msg_video);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        donateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // save data in presistence storage
                PresistenceData.ActiveCamp(getApplicationContext(),sr);
                Intent intent=new Intent(getApplicationContext(), Paymain2.class);
                startActivity(intent);
            }
        });
        // click on share button
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        if(msg_image.equals("NA"))
        {
            Intent intent2=new Intent(Intent.ACTION_SEND);
            intent2.setType("text/*");
            //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
            String data = "Lufa World \n\n "+msg_title+"\n\n"+msg_desc+" \n \n***********\n "+"Donation Target $"+target_amount+"\n"+"Achieved Target $"+rec_amount+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" +getPackageName());
            intent2.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent2.putExtra(Intent.EXTRA_TEXT,data);
            intent2.setPackage(Config.whatsapp_package);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // for particular choose we will set getPackage()
            /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
            startActivity(intent2);
        }
        else
        {
            PackageManager manager = getPackageManager();
            Picasso.get().load(msg_image).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    /* uri1=Uri.parse(Paths+File.separator+"10.jpg");*/
                    Intent intent=new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                    String data = "Lufa World \n\n "+msg_title+"\n\n"+msg_desc+" \n \n***********\n "+"Donation Target $"+target_amount+"\n"+"Achieved Target $"+rec_amount+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName());
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_TEXT,data);
                    intent.putExtra(Intent.EXTRA_STREAM,getLocalBitmapUri(bitmap,getApplicationContext()));
                    intent.setPackage(Config.whatsapp_package);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // for particular choose we will set getPackage()
                    /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
                    startActivity(intent);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    Toast.makeText(getApplicationContext(), "Load Image Failed", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });

            // end Share code

        }
            }
        });
        


    } //onCreate closer

    private Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri uriimg = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LufaWorld" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 50, out);
            out.close();
            /*uriimg = Uri.fromFile(file);*/
            uriimg = FileProvider.getUriForFile(Objects.requireNonNull(context),
                    BuildConfig.APPLICATION_ID + ".provider", file);
            refreshGallery(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uriimg;
    }

    private void refreshGallery(File file) {
        Intent intent1 = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent1.setData(Uri.fromFile(file));
        Objects.requireNonNull(getApplicationContext()).sendBroadcast(intent1);
    }

    private void timer(Integer min) {
        minn=min;
        (new Thread(new Runnable()
        {

            @Override
            public void run()
            {

                while (w<= minn)
                    try
                    {

                        Thread.sleep(60000);
                        runOnUiThread(new Runnable() // start actions in UI thread
                        {

                            @Override
                            public void run()
                            {
                                minn=minn-1;

                                if(!day.equals("0"))
                                {
                                    if(!hr.equals("0")) {
                                        dayLeft.setText(day + " Days : " + hr + " Hours : " + minn + " Min. Left");
                                    }
                                    else
                                    {
                                        dayLeft.setText(minn+" Min. Left");
                                    }
                                }
                                else
                                {
                                    dayLeft.setText(hr+" Hours : "+minn+" Min. Left");
                                }


                                w++;

                            }
                        });
                    }
                    catch (InterruptedException e)
                    {
                        // ooops
                    }
            }
        })).start();
    }

    private void loadActiveCampaign(final String page, final String lastid,final String camp_sr) {
        pd.show();
        Cache cache1 = new DiskBasedCache(Objects.requireNonNull(getApplicationContext()).getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());
        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.given_active_campaign, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseData(response,page);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("camp_sr",camp_sr);
                mydata.put("pageFrom",page);
                mydata.put("lastid",lastid);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData(String response, String page) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response);

            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    sr = jsonObject1.getString("sr");
                    String camp_sr = jsonObject1.getString("camp_sr");
                    String start_date = jsonObject1.getString("start_date");
                    String start_time = jsonObject1.getString("start_time");
                    String stop_date = jsonObject1.getString("stop_date");
                    String stop_time = jsonObject1.getString("stop_time");
                    target_amount = jsonObject1.getString("target_amount");
                    msg_title = jsonObject1.getString("msg_title");
                    msg_desc = jsonObject1.getString("msg_desc");
                    msg_image = jsonObject1.getString("msg_image");
                    msg_video = jsonObject1.getString("msg_video");
                    String username = jsonObject1.getString("username");
                    rec_amount = jsonObject1.getString("rec_amount");
                    min = jsonObject1.getString("min");
                    hr = jsonObject1.getString("hr");
                    day = jsonObject1.getString("day");

                    pd.dismiss();

                    // set Constraints
                    healine.setText(msg_title);
                    desc.setText(msg_desc);
                    Picasso.get().load(msg_image).into(imageView);
                    achieveText.setText("$"+rec_amount+" raised of $"+target_amount);
                    donateProgress.setMax(Integer.parseInt(target_amount));
                    donateProgress.setProgress(Integer.parseInt(rec_amount));
                    noFees.setText("No Fees");
                    
                    // time and date
                    if(!day.equals("0"))
                    {
                        if(!hr.equals("0")) {
                            dayLeft.setText(day + " Days : " + hr + " Hours : " + min + " Min. Left");
                        }
                        else
                        {
                            dayLeft.setText(min+" Min. Left");
                        }
                    }
                    else
                    {
                        dayLeft.setText(hr+" Hours : "+min+" Min. Left");
                    }
                    // set Parameters of Image view only and only when we will receive the url of image / in case of NA visibility gone
                    if(msg_image.equals("NA"))
                    {
                        imageView.setVisibility(View.GONE);
                    }
                    else
                    {
                        imageView.setVisibility(View.VISIBLE);
                    }

                    // set video id if video id is not available then we will receive NA and then hide visibility of Thumnail
                    if(msg_video.equals("NA"))
                    {
                        youtubeImage.setVisibility(View.GONE);
                        playIcon.setVisibility(View.GONE);
                    }
                    else {
                        youtubeImage.setVisibility(View.VISIBLE);
                        playIcon.setVisibility(View.VISIBLE);
                        final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                            }

                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                youTubeThumbnailView.setVisibility(View.VISIBLE);

                            }
                        };

                        youtubeImage.initialize(Config.API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                            @Override
                            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                                youTubeThumbnailLoader.setVideo(msg_video);
                                youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
                                // youTubeThumbnailLoader.release();

                            }

                            @Override
                            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                                //write something for failure
                            }
                        });
                    }
                    // start handler
                    timer(Integer.parseInt(min));

                }
            }
            else
            {
                Toast.makeText(this, "Network Issue", Toast.LENGTH_SHORT).show();
            }



        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
