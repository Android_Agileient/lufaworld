package com.lufaword;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.lufaword.Configuration.Config;
import com.lufaword.ExtraScreens.ExtraSearch;
import com.lufaword.Fragments.ExampleBottomDialogue;
import com.lufaword.Fragments.Tab1;
import com.lufaword.Fragments.Tab2;
import com.lufaword.Fragments.Tab3;
import com.lufaword.Fragments.Tab4;
import com.lufaword.Friends.Frnds;
import com.lufaword.Friends.MyFriends;
import com.lufaword.Friends.SearchFrnd;
import com.lufaword.Login.Aboutus;
import com.lufaword.Login.Terms;
import com.lufaword.Profile.MainProfile;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class MainActivity extends AppCompatActivity implements Tab1.OnFragmentInteractionListener, Tab2.OnFragmentInteractionListener, Tab3.OnFragmentInteractionListener, Tab4.OnFragmentInteractionListener,NavigationView.OnNavigationItemSelectedListener, ExampleBottomDialogue.BottomSheetListener {
    AlertDialog.Builder builder;
    PackageManager manager;
    public static BottomNavigationView navigationView;
    String data = "Agieleint";
    private LinearLayout mainLay1;
    private TabLayout tabLay;
    public static ViewPager viewPager2;
    private CircleImageView imageView9;
    private TextView sliderUserName;

    private CircleImageView imageChange;
    public static DrawerLayout drawer;
private String cameravalue="1";
    int GALLERY2 = 3;
    String image="";

    RequestQueue queue1,queue;
    ProgressDialog pd;
    private ImageView gif;
    private LinearLayout searchLay;
    private LinearLayout logo;
    private ImageView fakeSearch;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        builder = new AlertDialog.Builder(MainActivity.this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        mainLay1 = (LinearLayout) findViewById(R.id.mainLay1);
        tabLay = (TabLayout) findViewById(R.id.tabLay);
        viewPager2 = (ViewPager) findViewById(R.id.viewPager2);
        searchLay = (LinearLayout) findViewById(R.id.search_lay);
        logo = (LinearLayout) findViewById(R.id.logo);
        fakeSearch = (ImageView) findViewById(R.id.fake_search);

        pd=new ProgressDialog(this);
        pd.setTitle("Collecting data...");
        pd.setMessage("Please wait...");

// get intent
        Intent intent=getIntent();
        String value=intent.getStringExtra("value");
        if(value!=null)
        {
            if(value.equals('2'))
            {
                viewPager2.setCurrentItem(2);
            }
            else if(value.equals('1'))
            {
                viewPager2.setCurrentItem(1);
            }
            else if(value.equals('1'))
            {
                viewPager2.setCurrentItem(1);
            }
        }

        // animation image

        //Picasso.get().load(GetPresistenceData.getProfilepic(getApplicationContext())).into(imageChange);

        //setSupportActionBar(toolbar);
       /* FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationview = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationview.setNavigationItemSelectedListener(this);

        // Navigation view links
        // Navigation Views
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.social:
                        viewPager2.setCurrentItem(0);
                        return true;
                    case R.id.campaign:
                        viewPager2.setCurrentItem(1);
                        return true;
                    case R.id.events:
                        viewPager2.setCurrentItem(2);
                        return true;
                    case R.id.alerts:
                        viewPager2.setCurrentItem(3);
                        return true;
                }
                return false;
            }
        });

        View hView =  navigationview.inflateHeaderView(R.layout.nav_header_main);
        gif = (ImageView) hView.findViewById(R.id.gif);
        imageView9 = (CircleImageView) hView.findViewById(R.id.imageView);
        sliderUserName = (TextView) hView.findViewById(R.id.slider_user_name);
        String photo=GetPresistenceData.getProfilepic(getApplicationContext());
        String NamE=GetPresistenceData.getName(getApplicationContext());
        Glide.with(getApplicationContext()).asGif().load(R.drawable.circles).into(gif);
        assert NamE != null;
        if(!(NamE ==null) || !NamE.equals("") || !(photo ==null) || !(photo.length() <=10)) {
            Picasso.get().load(GetPresistenceData.getProfilepic(getApplicationContext())).into(imageView9);
            sliderUserName.setText(GetPresistenceData.getName(getApplicationContext()));
        }

        // first make Adapeter class and then set
        com.lufaword.Fragments.PagerAdapter pagerAdapter=new com.lufaword.Fragments.PagerAdapter(getSupportFragmentManager(),tabLay.getTabCount());
        viewPager2.setAdapter(pagerAdapter);
        // add page change listner on viewpager for slide
        viewPager2.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLay));
        tabLay.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        // Navigation view closed



        // click on fake search
        fakeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(MainActivity.this,ExtraSearch.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               startActivity(intent);
            }
        });


    } //onCreate closer


   /* @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA && data != null) {
//imgage captured
            if(cameravalue.equals("1")) {
                Bitmap bitmap = (Bitmap) (Objects.requireNonNull(data.getExtras())).get("data");
                assert bitmap != null;
                imageToString3(bitmap);
            }

        }


        else if (requestCode == GALLERY2 && data != null && data.getData() != null ) {
            if (resultCode == RESULT_OK) {

                Uri contentURI = data.getData();
                Log.e("Internal Uri", String.valueOf(contentURI));
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);
                    Log.e("bitmap....", String.valueOf(bitmap));
                    imageToString4(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                //sendData();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Failed to select the image.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to select image",
                        Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void imageToString4(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        Log.e("base64 Conversion",image);
        pd.dismiss();

        if(!(image == null))
        {
            upload_profile_dp(image,"1");
        }

    }

    private void imageToString3(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        pd.dismiss();
        Log.e("value",image);


        if(!(image == null))
        {
            upload_profile_dp(image,"1");
        }
    }

    private void upload_profile_dp(final String data, final String type) {
        Cache cache1 = new DiskBasedCache(getApplicationContext().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.update_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                dp_response(response1, data);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));
                mydata.put("email",GetPresistenceData.getEmail(getApplicationContext()));
                mydata.put("type",type);
                mydata.put("data",data);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void dp_response(String response1, String data) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String dp = jsonObject1.getString("dp");
                    pd.dismiss();
                    //new fetchProfileDate().execute("","","");
                    Toast.makeText(getApplicationContext(), "Updated profile image", Toast.LENGTH_SHORT).show();
                    PresistenceData.saveProfile(getApplicationContext(), GetPresistenceData.getMyIMEI(getApplicationContext()),GetPresistenceData.getName(getApplicationContext()), GetPresistenceData.getUsername(getApplicationContext()), GetPresistenceData.getEmail(getApplicationContext()),dp,GetPresistenceData.getPassword(getApplicationContext()));
                    Picasso.get().load(dp).into(Tab1.imageChange);
                    //Picasso.get().load(dp).into(backImg);
                    Tab1.uploadtoserverr(dp,"Update current profile image",GetPresistenceData.getMyIMEI(getApplicationContext()),GetPresistenceData.getEmail(getApplicationContext()),GetPresistenceData.getName(getApplicationContext()),getApplicationContext());

                }
            }
            else{
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent=new Intent(MainActivity.this, MainProfile.class);
            startActivity(intent);
        }
        else if (id == R.id.change_dp) {
            cameravalue="1";
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);

        }
        else if (id == R.id.dp_gal) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, GALLERY2);
        }
        else if (id == R.id.nav_gallery) {
            PresistenceData.saveIMEI(getApplicationContext(),null);
            PresistenceData.checkRegistration(getApplicationContext(),"0");
            PresistenceData.checkLogon(getApplicationContext(),"0");
            PresistenceData.saveProfile(getApplicationContext(),null,null,null,null,null,null);

            Intent intent=new Intent(MainActivity.this,Splash.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

        }
        else if (id == R.id.friends) {

            Intent intent=new Intent(MainActivity.this, MyFriends.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

        }else if (id == R.id.nav_share) {
            Toast.makeText(MainActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_send) {

            Intent intent=new Intent(MainActivity.this, Terms.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else if (id == R.id.about) {

            Intent intent=new Intent(MainActivity.this, Aboutus.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    // create instance for close the app
    @SuppressLint("StaticFieldLeak")
    public static MainActivity fs;
    // on Application closer send to share
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        String pv=String.valueOf(viewPager2.getCurrentItem());
        if(!pv.equals("0"))
        {
            viewPager2.setCurrentItem(0);

        }
        else {
            builder.setMessage("Do you want to close this app ?")
                    .setCancelable(true)
                    .setNeutralButton("Share", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            manager = getApplicationContext().getPackageManager();
                            Picasso.get().load(R.drawable.logo).into(new Target() {
                                                                         @Override
                                                                         public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                                             /* uri1=Uri.parse(Paths+File.separator+"10.jpg");*/
                                                                             Intent intent = new Intent(Intent.ACTION_SEND);
                                                                             intent.setType("image/*");
                                                                             //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                                                                             intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                                                             intent.putExtra(Intent.EXTRA_TEXT, data + "market://details?id=" + getApplicationContext().getPackageName());
                                                                             intent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, getApplicationContext()));

                                                                             intent.setPackage("com.whatsapp");
                                                                             // for particular choose we will set getPackage()
                                                                             //startActivity(Intent.createChooser(intent,"Share Via"));// this code use for universal sharing
                                                                             startActivity(intent);
                                                                         }

                                                                         @Override
                                                                         public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                                                             Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                                                         }

                                                                         @Override
                                                                         public void onPrepareLoad(Drawable placeHolderDrawable) {

                                                                         }
                                                                     }
                            );

                            // end Share code

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // MainActivity.fs.finish();
                            dialog.cancel();
                            /*  System.exit(0);*/
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // MainActivity.fs.finish();
                            finishAffinity();
                            /*  System.exit(0);*/
                        }
                    });
            //Creating dialog box
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("Exit");
            alert.show();

        }
            return super.onKeyDown(keyCode, event);
    }

    private Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri uriimg = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LufaWorld" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 50, out);
            out.close();
            /*uriimg = Uri.fromFile(file);*/
            uriimg = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                    BuildConfig.APPLICATION_ID + ".provider", file);
            refreshGallery(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uriimg;
    }
    private void refreshGallery(File file) {
        Intent intent1 = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent1.setData(Uri.fromFile(file));
        Objects.requireNonNull(getApplicationContext()).sendBroadcast(intent1);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onButtonClicked(String text) {
        Tab1.universal(text,getApplicationContext());
}



}
