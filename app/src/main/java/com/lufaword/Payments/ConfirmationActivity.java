package com.lufaword.Payments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.Fragments.Tab2;
import com.lufaword.R;
import com.lufaword.Social.Posts;
import com.lufaword.Social.RPOST;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_activity);


        //Getting Intent
        Intent intent = getIntent();


        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));

            //Displaying payment details
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {
        //Views
        TextView textViewId = (TextView) findViewById(R.id.paymentId);
        TextView textViewStatus= (TextView) findViewById(R.id.paymentStatus);
        TextView textViewAmount = (TextView) findViewById(R.id.paymentAmount);
        ImageView alertImage = (ImageView) findViewById(R.id.alert_image);

        if(jsonDetails.getString("state").equals("approved"))
        {
            //Showing the details from json object
            alertImage.setImageDrawable(getResources().getDrawable(R.drawable.done_tick));
            textViewId.setText(jsonDetails.getString("id"));
            textViewStatus.setText(jsonDetails.getString("state"));
            textViewAmount.setText(paymentAmount + " USD");
            String trans_id = jsonDetails.getString("id");
            String trans_state = jsonDetails.getString("state");
            String payment_amount = paymentAmount;

            saveTransactionHistory(trans_id, trans_state, payment_amount);
        }
        else {
            alertImage.setImageDrawable(getResources().getDrawable(R.drawable.alert));
            //Showing the details from json object
            textViewId.setText(jsonDetails.getString("id"));
            textViewStatus.setText(jsonDetails.getString("state"));
            textViewAmount.setText(paymentAmount + " USD");
            String trans_id = jsonDetails.getString("id");
            String trans_state = jsonDetails.getString("state");
            String payment_amount = paymentAmount;

            saveTransactionHistory(trans_id, trans_state, payment_amount);
        }
    }

    private void saveTransactionHistory(final String trans_id, final String trans_state, final String payment_amount) {
        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.save_transaction, new Response.Listener<String>() {
            @Override
            public void onResponse(String response3) {
                parseData3(response3);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ConfirmationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));
                mydata.put("trans_id", trans_id);
                mydata.put("trans_state", trans_state);
                mydata.put("payment_amount", payment_amount);
                mydata.put("camp_id", GetPresistenceData.getCapsr(getApplicationContext()));

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData3(String response3) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response3);

            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {

                Toast.makeText(this, "Thanks to donate to Lufaworld", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "Failed to save transactions", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


}