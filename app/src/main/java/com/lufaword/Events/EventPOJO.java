package com.lufaword.Events;

class EventPOJO {
    private String sr,title,desc1,type,cat,owner,start_time,start_date,stop_time,stop_date;
    EventPOJO(String sr, String title, String desc1, String type, String cat, String owner, String start_time, String start_date, String stop_time, String stop_date) {
    this.sr=sr;
    this.title=title;
    this.desc1=desc1;
    this.type=type;
    this.cat=cat;
    this.owner=owner;
    this.start_time=start_time;
    this.start_date=start_date;
    this.stop_time=stop_time;
    this.stop_date=stop_date;
    }
    // Getter and setter method

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getStop_date() {
        return stop_date;
    }

    public void setStop_date(String stop_date) {
        this.stop_date = stop_date;
    }
}
