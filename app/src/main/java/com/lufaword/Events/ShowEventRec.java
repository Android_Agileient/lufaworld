package com.lufaword.Events;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lufaword.R;

import java.util.ArrayList;

public class ShowEventRec extends RecyclerView.Adapter<ShowEventRec.Holder> {
    private ArrayList<EventPOJO>eventPOJOS;
    private Context mcontext;

    ShowEventRec(Context mcontext, ArrayList<EventPOJO> eventPOJOS) {
        this.eventPOJOS=eventPOJOS;
        this.mcontext=mcontext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.my_events,parent,false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        // set constraints
        holder.eventTitle.setText(eventPOJOS.get(position).getTitle());
        holder.eventDesc.setText(eventPOJOS.get(position).getDesc1());
        holder.startDate.setText(eventPOJOS.get(position).getStart_date());
        holder.startTime.setText(eventPOJOS.get(position).getStart_time());
        holder.closeDate.setText(eventPOJOS.get(position).getStop_date());
        holder.closeTime.setText(eventPOJOS.get(position).getStop_time());
        holder.owner.setText(eventPOJOS.get(position).getOwner());
    }


    @Override
    public int getItemCount() {
        return eventPOJOS.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private TextView eventTitle;
        private TextView eventDesc;
        private TextView startDate;
        private TextView startTime;
        private TextView closeDate;
        private TextView closeTime;
        private TextView owner;





        Holder(@NonNull View itemView) {
            super(itemView);
            eventTitle = (TextView) itemView.findViewById(R.id.event_title);
            eventDesc = (TextView) itemView.findViewById(R.id.event_desc);
            startDate = (TextView) itemView.findViewById(R.id.start_date);
            startTime = (TextView) itemView.findViewById(R.id.start_time);
            closeDate = (TextView) itemView.findViewById(R.id.close_date);
            closeTime = (TextView) itemView.findViewById(R.id.close_time);
            owner = (TextView) itemView.findViewById(R.id.owner);
        }
    }
}
