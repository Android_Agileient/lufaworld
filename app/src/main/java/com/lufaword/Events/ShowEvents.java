package com.lufaword.Events;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;

import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowEvents extends AppCompatActivity {
    private RecyclerView recEvents;
ArrayList<EventPOJO>eventPOJOS;
String types,cat,ops;
    private LinearLayout alertLay;
    private TextView alertText;
    private ImageView backArrow;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_events);
        recEvents = (RecyclerView) findViewById(R.id.rec_events);
        alertLay = (LinearLayout) findViewById(R.id.alert_lay);
        alertText = (TextView) findViewById(R.id.alert_text);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        // initilize arraylist
        eventPOJOS=new ArrayList<>();
        eventPOJOS.clear();

        // Recieve events
        Intent intent=getIntent();
        types=intent.getStringExtra("types");
        cat=intent.getStringExtra("cat");
        ops=intent.getStringExtra("ops");


        assert ops != null;
        if(ops.equals("1"))
        {
            // 1 for Show events by Cat and Types
            // 2 for Show events by keywords
            // load events
            loadShowEvents(types,cat);
        }
        else
        {
            loadShowEvents2(cat);
        }
        // clcik  on Back Image
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    } //onCreate closer

    private void loadShowEvents2(final String cat) {
        RequestQueue queue2;
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue2=new RequestQueue(cache1,network1);
        queue2.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.keywords_events, new Response.Listener<String>() {
            @Override
            public void onResponse(String response2) {
                loadRes(response2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("keyword",cat);


                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);
    }

    private void loadShowEvents(final String types, final String cat) {
        RequestQueue queue2;
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue2=new RequestQueue(cache1,network1);
        queue2.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.all_events, new Response.Listener<String>() {
            @Override
            public void onResponse(String response2) {
                loadRes(response2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("types",types);
                mydata.put("cat", cat);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);
    }

    private void loadRes(String response2) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String sr = jsonObject1.getString("sr");
                    String title = jsonObject1.getString("title");
                    String desc1 = jsonObject1.getString("desc1");
                    String type = jsonObject1.getString("type");
                    String cat = jsonObject1.getString("cat");
                    String owner = jsonObject1.getString("owner");
                    String start_time = jsonObject1.getString("start_time");
                    String start_date = jsonObject1.getString("start_date");
                    String stop_time = jsonObject1.getString("stop_time");
                    String stop_date = jsonObject1.getString("stop_date");

                    EventPOJO eventPOJO = new EventPOJO(sr,title,desc1,type,cat,owner,start_time,start_date,stop_time,stop_date);
                    eventPOJOS.add(eventPOJO);
                    recEvents.setVisibility(View.VISIBLE);

                    ShowEventRec showEventRec = new ShowEventRec(getApplicationContext(), eventPOJOS);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    recEvents.setLayoutManager(layoutManager);
                    recEvents.setHasFixedSize(true);
                    recEvents.setAdapter(showEventRec);
                }
            }
            else
            {
                alertText.setText("Events not found");
                alertLay.setVisibility(View.VISIBLE);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
