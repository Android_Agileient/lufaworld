package com.lufaword.Alertss;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.lufaword.Fragments.Tab1;
import com.lufaword.Fragments.Tab3;
import com.lufaword.MainActivity;
import com.lufaword.R;

public class Evaluate extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setConstraint on PD
        pd=new ProgressDialog(this);
        pd.setTitle("Analyzing");
        pd.setMessage("Wait a movement...");
        pd.show();

        // receiving intents
        Intent intent=getIntent();
        String value=intent.getStringExtra("value");

        assert value != null;
        switch (value) {
            case "1":
                pd.dismiss();
             Intent intent1=new Intent(this,MainActivity.class);
             startActivity(intent1);

                break;
            case "2":
                pd.dismiss();
                Intent intent2=new Intent(this,MainActivity.class);
                startActivity(intent2);
                break;
            case "3":
                pd.dismiss();
                Intent intent3=new Intent(this,MainActivity.class);
                startActivity(intent3);
                break;
            case "4":
                pd.dismiss();
                Intent intent4=new Intent(this,MainActivity.class);
                startActivity(intent4);
                break;
        }


    } //oncreate closer


}
