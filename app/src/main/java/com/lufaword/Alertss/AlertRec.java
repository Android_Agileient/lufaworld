package com.lufaword.Alertss;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lufaword.ExtraScreens.ActiveCamp;
import com.lufaword.R;

import java.util.ArrayList;

public class AlertRec extends RecyclerView.Adapter<AlertRec.Holder> {
    ArrayList<Alrt> alert;
    Context mcontext;
    public AlertRec(Context mcontext, ArrayList<Alrt> alert) {
        this.alert=alert;
        this.mcontext=mcontext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.alert_lay,parent,false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        if(!alert.get(position).getE_sr().equals("NA"))
        {
            holder.notes.setText("Events closing soon");
            holder.title.setText(alert.get(position).getE_title());
        }
        else if(!alert.get(position).getCamp_sr().equals("NA"))
        {
            holder.notes.setText("Campaign closing soon");
            holder.title.setText(alert.get(position).getCamp_msg_title());
        }
        else if(!alert.get(position).getTrans_sr().equals("NA"))
        {
            holder.notes.setText("You make recent transaction");
            holder.title.setText("Transaction id: "+alert.get(position).getTrans_id());
            holder.text2.setText("Amount: "+alert.get(position).getAmount());
            holder.text3.setText("Transaction Status: "+alert.get(position).getTrans_state());
            holder.text2.setVisibility(View.VISIBLE);
            holder.text3.setVisibility(View.VISIBLE);
        }


        // click on lay to open camp
        // type-camp,event,trans
        holder.alertLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(alert.get(position).getType().equals("camp"))
                {
                    Intent intent=new Intent(mcontext, ActiveCamp.class);
                    intent.putExtra("camp_sr",alert.get(position).getCamp_sr());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            }
        });

    }



    @Override
    public int getItemCount() {
        return alert.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView notes;
        private TextView title;
        private TextView text2;
        private TextView text3;
        private LinearLayout alertLay;




        public Holder(@NonNull View itemView) {
            super(itemView);
            notes = (TextView) itemView.findViewById(R.id.notes);
            title = (TextView) itemView.findViewById(R.id.title);
            text2 = (TextView) itemView.findViewById(R.id.text2);
            text3 = (TextView) itemView.findViewById(R.id.text3);
            alertLay = (LinearLayout) itemView.findViewById(R.id.alert_lay);

        }
    }
}
