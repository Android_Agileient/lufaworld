package com.lufaword.Alertss;

public class Alrt {

    private String e_sr,e_title,e_desc1,e_stop_date,camp_sr,camp_stop_date,camp_target_amount,camp_msg_title,camp_msg_desc,camp_rec_amount,trans_sr,trans_id,trans_state,amount,type;
    public Alrt(String e_sr, String e_title, String e_desc1, String e_stop_date, String camp_sr, String camp_stop_date, String camp_target_amount, String camp_msg_title, String camp_msg_desc, String camp_rec_amount, String trans_sr, String trans_id, String trans_state, String amount,String type) {
    this.e_sr=e_sr;
    this.e_title=e_title;
    this.e_desc1=e_desc1;
    this.e_stop_date=e_stop_date;
    this.camp_sr=camp_sr;
    this.camp_stop_date=camp_stop_date;
    this.camp_target_amount=camp_target_amount;
    this.camp_msg_title=camp_msg_title;
    this.camp_msg_desc=camp_msg_desc;
    this.camp_rec_amount=camp_rec_amount;
    this.trans_sr=trans_sr;
    this.trans_id=trans_id;
    this.trans_state=trans_state;
    this.amount=amount;
    this.type=type;


    }
    // getter and setter method


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getE_sr() {
        return e_sr;
    }

    public void setE_sr(String e_sr) {
        this.e_sr = e_sr;
    }

    public String getE_title() {
        return e_title;
    }

    public void setE_title(String e_title) {
        this.e_title = e_title;
    }

    public String getE_desc1() {
        return e_desc1;
    }

    public void setE_desc1(String e_desc1) {
        this.e_desc1 = e_desc1;
    }

    public String getE_stop_date() {
        return e_stop_date;
    }

    public void setE_stop_date(String e_stop_date) {
        this.e_stop_date = e_stop_date;
    }

    public String getCamp_sr() {
        return camp_sr;
    }

    public void setCamp_sr(String camp_sr) {
        this.camp_sr = camp_sr;
    }

    public String getCamp_stop_date() {
        return camp_stop_date;
    }

    public void setCamp_stop_date(String camp_stop_date) {
        this.camp_stop_date = camp_stop_date;
    }

    public String getCamp_target_amount() {
        return camp_target_amount;
    }

    public void setCamp_target_amount(String camp_target_amount) {
        this.camp_target_amount = camp_target_amount;
    }

    public String getCamp_msg_title() {
        return camp_msg_title;
    }

    public void setCamp_msg_title(String camp_msg_title) {
        this.camp_msg_title = camp_msg_title;
    }

    public String getCamp_msg_desc() {
        return camp_msg_desc;
    }

    public void setCamp_msg_desc(String camp_msg_desc) {
        this.camp_msg_desc = camp_msg_desc;
    }

    public String getCamp_rec_amount() {
        return camp_rec_amount;
    }

    public void setCamp_rec_amount(String camp_rec_amount) {
        this.camp_rec_amount = camp_rec_amount;
    }

    public String getTrans_sr() {
        return trans_sr;
    }

    public void setTrans_sr(String trans_sr) {
        this.trans_sr = trans_sr;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getTrans_state() {
        return trans_state;
    }

    public void setTrans_state(String trans_state) {
        this.trans_state = trans_state;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
