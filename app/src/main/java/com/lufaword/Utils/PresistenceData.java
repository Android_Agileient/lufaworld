package com.lufaword.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class PresistenceData {
    public static void saveIMEI(Context mcontext, String IMEI) {

        SharedPreferences.Editor editor = mcontext.getSharedPreferences("myimei", MODE_PRIVATE).edit();
        editor.putString("imei", IMEI);
        editor.apply();
    }
    // check registration state, registered or not
    public static void checkRegistration(Context mcontext, String res) {

        SharedPreferences.Editor editor = mcontext.getSharedPreferences("registration", MODE_PRIVATE).edit();
        editor.putString("response", res);
        editor.apply();
    }

    // check user is login or not
    public static void checkLogon(Context mcontext, String res) {

        SharedPreferences.Editor editor = mcontext.getSharedPreferences("logon", MODE_PRIVATE).edit();
        editor.putString("status", res);
        editor.apply();
    }

    // Save Festivals Presistence
    public static void Festivals(Context mcontext, String fest_img) {
        SharedPreferences.Editor editor = mcontext.getSharedPreferences("festivals", MODE_PRIVATE).edit();
        editor.putString("fest_img",fest_img);
        editor.apply();
    }
    // Save Close Festivals click Presistence
    public static void CloseFest(Context mcontext,String closeFest,String date1) {
        SharedPreferences.Editor editor = mcontext.getSharedPreferences("close_fest", MODE_PRIVATE).edit();
        editor.putString("closeFest",closeFest);
        editor.putString("date",date1);
        editor.apply();
    }

    // Save Close Festivals click Presistence
    public static void saveProfile(Context mcontext,String imei,String name,String username,String email, String profile_pic,String password) {
        SharedPreferences.Editor editor = mcontext.getSharedPreferences("profile", MODE_PRIVATE).edit();
        editor.putString("imei",imei);
        editor.putString("name",name);
        editor.putString("username",username);
        editor.putString("email",email);
        editor.putString("profile_pic",profile_pic);
        editor.putString("password",password);
        editor.apply();
    }

    // Save current Active camp_sr
    public static void ActiveCamp(Context mcontext,String sr) {
        SharedPreferences.Editor editor = mcontext.getSharedPreferences("active_camp_sr", MODE_PRIVATE).edit();
        editor.putString("camp_sr",sr);
        editor.apply();
    }


}
