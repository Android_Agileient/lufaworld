package com.lufaword.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class GetPresistenceData {

    // check logon now if registered
    public static String getMyLogon(Context context) {
        SharedPreferences checkLogon = context.getSharedPreferences("logon", MODE_PRIVATE);
        String myLogon = checkLogon.getString("status", "0");
        return myLogon;
    }

    // collect imei details
    public static String getMyIMEI(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("myimei", MODE_PRIVATE);
        String imei = prefs.getString("imei", null);
        return imei;
    }
    // check user is registered or not
    public static String getMyRegStatus(Context context) {
        // collect imei details
        SharedPreferences prefs2 = context.getSharedPreferences("registration", MODE_PRIVATE);
        String myLogin_status = prefs2.getString("response", null);
        return myLogin_status;
    }


    // Get Festivals image
    public static String getFestivals(Context context) {
        SharedPreferences fest_img = context.getSharedPreferences("festivals", MODE_PRIVATE);
        return fest_img.getString("fest_img", null);
    }

    // Get Close Festivals click
    public static String getCloseFest(Context context) {
        SharedPreferences closeFest = context.getSharedPreferences("close_fest", MODE_PRIVATE);
        return closeFest.getString("closeFest", null);
    }

    // Get Close Festivals click
    public static String getCloseFestDate(Context context) {
        SharedPreferences closeFest = context.getSharedPreferences("close_fest", MODE_PRIVATE);
        return closeFest.getString("date", null);
    }
    // collect profile data
    // collect imei details
    public static String getName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("profile", MODE_PRIVATE);
        String name3 = prefs.getString("name", null);
        return name3;
    }
    public static String getUsername(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("profile", MODE_PRIVATE);
        String username = prefs.getString("username", null);
        return username;
    }
    public static String getEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("profile", MODE_PRIVATE);
        String email = prefs.getString("email", null);
        return email;
    }
    public static String getProfilepic(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("profile", MODE_PRIVATE);
        String profile_pic = prefs.getString("profile_pic", null);
        return profile_pic;
    }
    public static String getPassword(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("profile", MODE_PRIVATE);
        String password = prefs.getString("password", null);
        return password;
    }

    public static String getCapsr(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("active_camp_sr", MODE_PRIVATE);
        String camp_sr = prefs.getString("camp_sr", null);
        return camp_sr;
    }
}
