package com.lufaword.Report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lufaword.Alertss.AlertRec;
import com.lufaword.R;

import java.util.ArrayList;

class RecTrans extends RecyclerView.Adapter<RecTrans.Holder> {
    Context mcontext;
    ArrayList<MyTrans> myTrans1;
    RecTrans(Context mcontext, ArrayList<MyTrans> myTrans1) {
        this.mcontext=mcontext;
        this.myTrans1=myTrans1;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.trans_lay,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        // set Constraints
        holder.transId.setText("Trans. ID: "+myTrans1.get(position).getTrans_id());
        holder.transState.setText("Trans. status: "+myTrans1.get(position).getTrans_state());
        holder.payDate.setText(myTrans1.get(position).getDate1());
        holder.payTime.setText(myTrans1.get(position).getTime1());
        holder.amount.setText(myTrans1.get(position).getAmount());


    }


    @Override
    public int getItemCount() {
        return myTrans1.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView transId;
        private TextView transState;
        private TextView payDate;
        private TextView payTime;
        private TextView amount;



        Holder(@NonNull View itemView) {
            super(itemView);
            transId = (TextView) itemView.findViewById(R.id.trans_id);
            transState = (TextView) itemView.findViewById(R.id.trans_state);
            payDate = (TextView) itemView.findViewById(R.id.pay_date);
            payTime = (TextView) itemView.findViewById(R.id.pay_time);
            amount = (TextView) itemView.findViewById(R.id.amount);

        }
    }
}
