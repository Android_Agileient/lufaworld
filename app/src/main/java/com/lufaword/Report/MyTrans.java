package com.lufaword.Report;

class MyTrans {
    private String sr,trans_id,trans_state,amount,date1,time1;
    MyTrans(String sr, String trans_id, String trans_state, String amount, String date1, String time1) {
        this.sr=sr;
        this.trans_id=trans_id;
        this.trans_state=trans_state;
        this.amount=amount;
        this.date1=date1;
        this.time1=time1;
    }
    // getter and setter method

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getTrans_state() {
        return trans_state;
    }

    public void setTrans_state(String trans_state) {
        this.trans_state = trans_state;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }
}
