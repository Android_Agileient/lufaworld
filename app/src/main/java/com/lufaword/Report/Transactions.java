package com.lufaword.Report;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lufaword.Configuration.Config;
import com.lufaword.Login.ResetUsername;
import com.lufaword.Profile.MainProfile;
import com.lufaword.R;

import com.lufaword.Splash;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Transactions extends AppCompatActivity {
    private ImageView GoBack;
    private TextView headerTitle;
    private RecyclerView transRec;
    private LinearLayout alertLay;
    private TextView alertText;
    RequestQueue queue;
    ArrayList<MyTrans>myTrans1;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transactions);
        GoBack = (ImageView) findViewById(R.id.GoBack);
        headerTitle = (TextView) findViewById(R.id.header_title);
        transRec = (RecyclerView) findViewById(R.id.trans_rec);
        alertLay = (LinearLayout) findViewById(R.id.alert_lay);
        alertText = (TextView) findViewById(R.id.alert_text);

        // initilize array
        myTrans1=new ArrayList<>();
        myTrans1.clear();
        // set Constraints
        headerTitle.setText("MY TRANSACTIONS");

        // click on GoBack
        GoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Transactions.this, MainProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        loadTransaction(GetPresistenceData.getMyIMEI(getApplicationContext()));


    } // onCreate closer

    private void loadTransaction(String myIMEI) {
        queue = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
        StringRequest request = new StringRequest(Request.Method.POST, Config.my_transactions, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData1(response1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",GetPresistenceData.getMyIMEI(getApplicationContext()));
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData1(String response1) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String trans_id = jsonObject1.getString("trans_id");
                    String trans_state = jsonObject1.getString("trans_state");
                    String amount = jsonObject1.getString("amount");
                    String date1 = jsonObject1.getString("date1");
                    String time1 = jsonObject1.getString("time1");
                    String sr = jsonObject1.getString("sr");


                    MyTrans myTrans2 = new MyTrans(sr,trans_id,trans_state,amount,date1,time1);
                    myTrans1.add(myTrans2);
                    transRec.setVisibility(View.VISIBLE);
                    alertLay.setVisibility(View.GONE);

                    RecTrans recTrans = new RecTrans(getApplicationContext(), myTrans1);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    transRec.setLayoutManager(layoutManager);
                    transRec.setHasFixedSize(true);
                    transRec.setAdapter(recTrans);
                    //suggestFRec.scrollToPosition(5); //use to focus the item with index
                    //rSuugested.notifyDataSetChanged();

                }
            }
            else
            {

                transRec.setVisibility(View.GONE);
                alertLay.setVisibility(View.VISIBLE);
                alertText.setText("No transactions found");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
