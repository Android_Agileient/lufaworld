package com.lufaword.Configuration;

public class Config {
    public static final String ver = "http://lufaworld.com/API/version.php";
    public static final String saveRegistration = "http://lufaworld.com/API/save_registration.php";
    public static final String checkLogin = "http://lufaworld.com/API/check_login.php";
    public static final String resetPass = "http://lufaworld.com/API/reset_pass.php";
    public static final String savePost = "http://lufaworld.com/API/posts.php";
    public static final String showPost = "http://lufaworld.com/API/show_post.php";
    public static final String sharePost = "http://lufaworld.com/API/share_post.php";
    public static final String delete_post = "http://lufaworld.com/API/delete_post.php";
    public static final String save_like = "http://lufaworld.com/API/save_like.php";
    public static final String notification = "http://lufaworld.com/API/notification.php";
    public static final String save_Comment = "http://lufaworld.com/API/save_Comment.php";
    public static final String show_comments = "http://lufaworld.com/API/show_comments.php";
    public static final String update_profile = "http://lufaworld.com/API/update_profile.php";
    public static final String delete_comment = "http://lufaworld.com/API/delete_comment.php";
    public static final String suggest_frnds = "http://lufaworld.com/API/suggested_friends.php";
    public static final String active_campaign = "http://lufaworld.com/API/active_campaign.php";
    public static final String given_active_campaign = "http://lufaworld.com/API/given_active_campaign.php";
    public static final String API_KEY = "AIzaSyD7zzSWhQYUQhlxOZQs7I4TWIiJ03nBew4";
    public static final String add_friend = "http://lufaworld.com/API/add_friend.php";
    public static final String add_friends = "http://lufaworld.com/API/add_friends.php";
    public static final String all_friends = "http://lufaworld.com/API/all_users.php";
    public static final String frnd_list = "http://lufaworld.com/API/frnd_list.php";
    public static final String remove_frnd = "http://lufaworld.com/API/remove_friend.php";
    public static final String create_group = "http://lufaworld.com/API/create_group.php";
    public static final String my_groups = "http://lufaworld.com/API/my_groups.php";
    public static final String delete_group = "http://lufaworld.com/API/delete_group.php";
    public static final String leave_group = "http://lufaworld.com/API/leave_group.php";
    public static final String pulic_groups = "http://lufaworld.com/API/groups.php";
    public static final String join_group = "http://lufaworld.com/API/join_group.php";
    public static final String my_friends = "http://lufaworld.com/API/my_friend_list.php";
    public static final String update_post = "http://lufaworld.com/API/update_post.php";
    public static final String save_transaction = "http://lufaworld.com/API/saveTransactions.php";
    public static final String events_types = "http://lufaworld.com/API/events_type.php";
    public static final String category = "http://lufaworld.com/API/category.php";
    public static final String all_events = "http://lufaworld.com/API/events.php";
    public static final String keywords_events = "http://lufaworld.com/API/events_by_keywords.php";
    public static final String event_camp_alert = "http://lufaworld.com/API/event_camp_alert.php";
    public static final String frnd_profile_details = "http://lufaworld.com/API/friend_profile_details.php";
    public static final String unlike_post = "http://lufaworld.com/API/unlike_post.php";
    public static final String update_username = "http://lufaworld.com/API/update_username.php";
    public static final String my_transactions = "http://lufaworld.com/API/my_transactions.php";
    public static final String app_notification = "http://lufaworld.com/API/app_notification.php";
    public static final String recent_search = "http://lufaworld.com/API/search_recent.php";


    // Values
    public static final String whatsapp_package = "com.whatsapp";
    public static final String app_id = "com.lufaworld";
}
