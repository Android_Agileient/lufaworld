package com.lufaword.Fragments;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int nNoOftabs;
    public PagerAdapter(FragmentManager fm, int Numberoftabs)
    {
        super(fm);

        this.nNoOftabs=Numberoftabs;
    }





    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new Tab1();

            case 1:
                return new Tab2();
            case 2:
                return new Tab3();
            case 3:
                return new Tab4();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return nNoOftabs;
    }
}