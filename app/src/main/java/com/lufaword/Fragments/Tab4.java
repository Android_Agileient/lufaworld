package com.lufaword.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Alertss.AlertRec;
import com.lufaword.Alertss.Alrt;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab4.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab4#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab4 extends Fragment {
    private RecyclerView alertRec;
    private LinearLayout alertLay;
    private TextView alertText;
ArrayList<Alrt>alert;







    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Tab4() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab4.
     */
    // TODO: Rename and change types and number of parameters
    public static Tab4 newInstance(String param1, String param2) {
        Tab4 fragment = new Tab4();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alert=new ArrayList<>();
        alert.clear();


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_tab4, container, false);
        alertRec = (RecyclerView) view.findViewById(R.id.alert_rec);
        alertLay = (LinearLayout) view.findViewById(R.id.alert_lay);
        alertText = (TextView) view.findViewById(R.id.alert_text);

        loadAlerts(GetPresistenceData.getMyIMEI(getContext()));


        return view;
    }

    private void loadAlerts(String myIMEI) {
        RequestQueue queue2;
        Cache cache1 = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue2=new RequestQueue(cache1,network1);
        queue2.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.event_camp_alert, new Response.Listener<String>() {
            @Override
            public void onResponse(String response2) {
                loadRes(response2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei",GetPresistenceData.getMyIMEI(getContext()));
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);
    }

    private void loadRes(String response2) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String E_sr = jsonObject1.getString("event_sr");
                    String E_title = jsonObject1.getString("event_title");
                    String E_desc1 = jsonObject1.getString("event_desc1");
                    String E_stop_date = jsonObject1.getString("event_stop_date");
                    String camp_sr = jsonObject1.getString("camp_sr");
                    String camp_stop_date = jsonObject1.getString("camp_stop_date");
                    String camp_target_amount = jsonObject1.getString("camp_target_amount");
                    String camp_msg_title = jsonObject1.getString("camp_msg_title");
                    String camp_msg_desc = jsonObject1.getString("camp_msg_desc");
                    String camp_rec_amount = jsonObject1.getString("camp_rec_amount");
                    String trans_sr = jsonObject1.getString("trans_sr");
                    String trans_id = jsonObject1.getString("trans_id");
                    String trans_state = jsonObject1.getString("trans_state");
                    String amount = jsonObject1.getString("amount");
                    String type = jsonObject1.getString("type");





                    Alrt alerts = new Alrt(E_sr,E_title,E_desc1,E_stop_date,camp_sr,camp_stop_date,camp_target_amount,camp_msg_title,camp_msg_desc,camp_rec_amount,trans_sr,trans_id,trans_state,amount,type);
                    alert.add(alerts);
                    alertRec.setVisibility(View.VISIBLE);

                    AlertRec alertRec1 = new AlertRec(getContext(), alert);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    alertRec.setLayoutManager(layoutManager);
                    alertRec.setHasFixedSize(true);
                    alertRec.setAdapter(alertRec1);
                }
            }
            else
            {
                alertText.setText("No recent alerts found");
                alertLay.setVisibility(View.VISIBLE);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
