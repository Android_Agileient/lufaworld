package com.lufaword.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.Events.ShowEvents;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab3.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab3 extends Fragment  {
 /*   private static Spinner types;
    private Spinner cat;*/


    static ArrayAdapter<String> arrayAdapter;
    ArrayAdapter<String> arrayAdapter2;
    static ArrayList<String> ty=new ArrayList<>();
    ArrayList<String> ct=new ArrayList<>();
String dataTy;
    private TextView titleKeyword;
    private EditText keywordsTxt;
    private Button searchBtn;
    private LinearLayout container;
    //private ImageView arrowList;










    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Tab3() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab3.
     */
    // TODO: Rename and change types and number of parameters
    public static Tab3 newInstance(String param1, String param2) {
        Tab3 fragment = new Tab3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_tab3, container, false);
   /*     types = (Spinner) view.findViewById(R.id.types);
        cat = (Spinner) view.findViewById(R.id.cat);*/
        titleKeyword = (TextView) view.findViewById(R.id.title_keyword);
        keywordsTxt = (EditText) view.findViewById(R.id.keywords_txt);
        searchBtn = (Button) view.findViewById(R.id.search_btn);
        container = (LinearLayout) view.findViewById(R.id.container);
        //arrowList = (ImageView) view.findViewById(R.id.arrow_list);

        // initilize array
            ty.add(0,"Select Types");
            ct.add(0,"Select Category");
        // load types list
        //loadTypes(getActivity(), Objects.requireNonNull(getContext()));
/*
        // click on spinner
        types.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(!String.valueOf(i).equals("0"))
                {
                    dataTy=ty.get(i);
                    loadCat(ty.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
// click on cat spinner
    /*    cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!String.valueOf(i).equals("0"))
                {
                    String mycat=ct.get(i);

                    Intent intent=new Intent(getContext(), ShowEvents.class);
                    intent.putExtra("types",dataTy);
                    intent.putExtra("cat",mycat);
                    intent.putExtra("ops","1");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        // search by keywords click listner
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key=keywordsTxt.getText().toString();
                if(key.length()==0)
                {
                    Toast.makeText(getContext(), "Cant be null", Toast.LENGTH_SHORT).show();
                    keywordsTxt.requestFocus();
                }
                else if(key.length()<=2)
                {
                    Toast.makeText(getContext(), "Require more than 2 words", Toast.LENGTH_SHORT).show();
                    keywordsTxt.requestFocus();
                }
                else if(key.contains("Fuck") || key.contains("pussy") || key.contains("MotherFucker") || key.contains("dick") || key.contains("bastered") || key.contains("sexy"))
                {
                    Toast.makeText(getContext(), "Dont use abusing", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent intent=new Intent(getContext(),ShowEvents.class);
                    intent.putExtra("types","NA");
                    intent.putExtra("cat",key);
                    intent.putExtra("ops","2");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
        return view;
    }



    private void loadCat(final String type) {
        RequestQueue queue2;
        Cache cache1 = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue2=new RequestQueue(cache1,network1);
        queue2.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.category, new Response.Listener<String>() {
            @Override
            public void onResponse(String response2) {
                parseData2(response2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("types", type);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);
    }

    private void parseData2(String response2) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String cat2 = jsonObject1.getString("cat");


                    ct.add(cat2);

                    //Creating the ArrayAdapter instance having the country list
                    arrayAdapter2 = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,ct);
                    arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    //cat.setAdapter(arrayAdapter2);
                    //cat.setVisibility(View.VISIBLE);
                    //arrowList.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                Toast.makeText(getContext(), "I think you are not in network", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    public static void loadTypes(FragmentActivity activity, final Context context) {
        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.events_types, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData1(response1,context);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei", GetPresistenceData.getMyIMEI(context));

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private static void parseData1(String response1, Context context) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String types1 = jsonObject1.getString("types");


                    ty.add(types1);

                    //Creating the ArrayAdapter instance having the country list
                   arrayAdapter = new ArrayAdapter<>(Objects.requireNonNull(context),android.R.layout.simple_spinner_item,ty);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    //types.setAdapter(arrayAdapter);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
