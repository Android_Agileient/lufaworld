package com.lufaword.Fragments;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.lufaword.BuildConfig;
import com.lufaword.Configuration.Config;
import com.lufaword.Friends.FrndRec2;
import com.lufaword.Friends.Frnds;
import com.lufaword.Friends.MyFriends;
import com.lufaword.Friends.SearchFrnd;
import com.lufaword.MainActivity;
import com.lufaword.NetworkNot;
import com.lufaword.R;
import com.lufaword.Social.GroupOptions;
import com.lufaword.Social.Posts;
import com.lufaword.Social.RPOST;
import com.lufaword.Utils.AppStatus;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.media.MediaRecorder.VideoSource.CAMERA;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab1 extends Fragment implements ExampleBottomDialogue.BottomSheetListener {

    private ImageView backImg;
    private TextView changeDp;
    private TextView userName;
    ArrayList<String>arr=new ArrayList<>();
    private LinearLayout friends;
    private ImageView userFrnd;
    private TextView frndsTxt;
    private LinearLayout groupLay;
    private ImageView userGroup;
    private TextView groupTxt;
    private ImageView fakeCamera;
    public static TextView recomandedFriend;
    public static RecyclerView frndRec;
    private static TextView recomandedText;
    private static RecyclerView recPost;
    public static LinearLayout postLay;
   PackageManager manager;
    static ProgressDialog pd;
    private static final int REQUEST_STORAGE_PERMISSION = 100;
    private static final int REQUEST_PICK_PHOTO = 101;
    private static int overallXScroll = 0;
    private Uri uri;
    int GALLERY = 2,CAMERA2=3,GALLERY2 = 3;
    String image="";
    static RequestQueue queue;
    RequestQueue queue1;
    static ArrayList<Posts> posts;
    static String cameravalue="";
    public static CircleImageView imageChange;
    private Button enterMsg;
    private TextView cameraBtn;
    private TextView photo;
    private static LinearLayout postLay3;
    private TextView closePostLay;
    private TextView createPost;
    private TextView postBtn;
    private EditText shareIdeaText;
    private ImageView imgView;
    private TextView photos;
    private TextView camera;


    int w;
    private static LinearLayout alertLay;
    private static TextView alertText;
    //private SwipeRefreshLayout swipe;

    static ArrayList<Frnds> Frnd;







    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Tab1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab1.
     */
    // TODO: Rename and change types and number of parameters
    private static Tab1 newInstance(String param1, String param2) {
        Tab1 fragment = new Tab1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // array initilization here


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.community,container,false);
        backImg = (ImageView) view.findViewById(R.id.backImg);
        //changeDp = (TextView) view.findViewById(R.id.change_dp);
        userName = (TextView) view.findViewById(R.id.user_name);
        friends = (LinearLayout) view.findViewById(R.id.friends);
        userFrnd = (ImageView) view.findViewById(R.id.user_frnd);
        frndsTxt = (TextView) view.findViewById(R.id.frnds_txt);
        groupLay = (LinearLayout) view.findViewById(R.id.group_lay);
        userGroup = (ImageView) view.findViewById(R.id.user_group);
        groupTxt = (TextView) view.findViewById(R.id.group_txt);
        //enterMsg = (EditText) view.findViewById(R.id.enter_msg);
        //recomandedFriend = (TextView) view.findViewById(R.id.recomandedFriend);
        //frndRec = (RecyclerView) view.findViewById(R.id.frnd_rec);
        recomandedText = (TextView) view.findViewById(R.id.recomandedText);
        recPost = (RecyclerView) view.findViewById(R.id.rec_post);
        postLay = (LinearLayout) view.findViewById(R.id.post_lay);
        shareIdeaText = (EditText) view.findViewById(R.id.share_idea_text);
        imgView = (ImageView) view.findViewById(R.id.img_view);
        alertLay = (LinearLayout) view.findViewById(R.id.alert_lay);
        alertText = (TextView) view.findViewById(R.id.alert_text);
        imageChange = (CircleImageView) view.findViewById(R.id.imageChange);
        enterMsg = (Button) view.findViewById(R.id.enter_msg);
        cameraBtn = (TextView) view.findViewById(R.id.camera_btn);
        photo = (TextView) view.findViewById(R.id.photo);
        postLay3 = (LinearLayout) view.findViewById(R.id.post_lay3);
        closePostLay = (TextView) view.findViewById(R.id.close_post_lay);
        createPost = (TextView) view.findViewById(R.id.create_post);
        postBtn = (TextView) view.findViewById(R.id.post_btn);
        shareIdeaText = (EditText) view.findViewById(R.id.share_idea_text);
        imgView = (ImageView) view.findViewById(R.id.img_view);
        photos = (TextView) view.findViewById(R.id.photos);
        camera = (TextView) view.findViewById(R.id.camera);





        //swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);

        // initilize pd
        pd=new ProgressDialog(getActivity());
        pd.setTitle("Loading Data...");
        pd.setMessage("Wait....");
        //pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
pd.show();
        // Initilize array
        posts=new ArrayList<>();
        Frnd=new ArrayList<>();
        posts.clear();

        // set constraints
        Picasso.get().load(GetPresistenceData.getProfilepic(getContext())).into(imageChange);



        // swipe on refresg
     /*   swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPost(getContext(),"T","T");
                swipe.setRefreshing(false);
            }
        });*/
        // load all Friends for auto complete text
       // new loadAllFriends().execute();
        // click on fake search button
    /*    fakeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fakeSearch.setVisibility(View.GONE);
                searchFrndFrameLay.setVisibility(View.VISIBLE);

                (new Thread(new Runnable()
                {

                    @Override
                    public void run()
                    {
                        while (w<=15)
                            try
                            {
                                Thread.sleep(20);
                                getActivity().runOnUiThread(new Runnable() // start actions in UI thread
                                {

                                    @Override
                                    public void run()
                                    {
                                        searchEditBox.setEms(w);
                                        w++;

                                    }
                                });
                            }
                            catch (InterruptedException e)
                            {
                                // ooops
                            }
                    }
                })).start(); // the while thread will start in BG thread

                searchEditBox.requestFocus();
            }
        });*/
        // click on real search button if editbox is null then visiblity gone and show fake search button


        enterMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLay3.setVisibility(View.VISIBLE);

            }
        });
        closePostLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLay3.setVisibility(View.GONE);

            }
        });

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constraints();
                captureimagefromcamera();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureimagefromcamera();
            }
        });
        shareIdeaText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shareIdeaText.requestFocus()==true)
                {
                    postBtn.setEnabled(true);
                }
            }
        });
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gallery();
            }
        });
        photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gallery();
            }
        });
        // click on frnd linear layout for friend list
        friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // friends.setBackground(getResources().getDrawable(R.drawable.circle_button));
                Intent intent=new Intent(getContext(), MyFriends.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        // click on Profile image
        imageChange.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                MainActivity.drawer.openDrawer(Gravity.START);
            }
        });


        // load posts

        if (AppStatus.getInstance(Objects.requireNonNull(getActivity())).isOnline()) {
            loadPost(getContext(),"T","T");

        }
        else
        {
            Intent intent=new Intent(getActivity(), NetworkNot.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        // click on enter msg to open post layout
     /*   enterMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLay.setVisibility(View.VISIBLE);
                searchLay.setVisibility(View.GONE);

            }
        });*/
/*// clcik on fake camera
        fakeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constraints();
                chooseImageOptions();
            }
        });*/
        //close the post layout on click on closePostLay
    /*    closePostLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLay.setVisibility(View.GONE);
                shareIdeaText.setText("");
            }
        });
*/
        // click on User group layout
    /*    groupLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //groupLay.setBackground(getResources().getDrawable(R.drawable.circle_button));
                Intent intent=new Intent(getContext(), GroupOptions.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });*/
        // send the post to server
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate status data
                String ideas_text=shareIdeaText.getText().toString();
                if(ideas_text.length()<3)
                {
                    Snackbar.make(getActivity().getWindow().getDecorView().getRootView(),"Your ideas are too small worded",Snackbar.LENGTH_SHORT).show();
                    shareIdeaText.requestFocus();
                }
                else if(ideas_text.length()>250)
                {
                    Snackbar.make(getActivity().getWindow().getDecorView().getRootView(),"Text limit maximum exceed than 250 words",Snackbar.LENGTH_SHORT).show();
                    shareIdeaText.requestFocus();
                }
                else
                {
                    if(image.equals(""))
                    {
                        image="NA";
                        String imei= GetPresistenceData.getMyIMEI(getActivity());
                        String email=GetPresistenceData.getEmail(getActivity());
                        String name=GetPresistenceData.getName(getActivity());
                        uploadtoserverr(image,ideas_text,imei,email,name,getContext());
                    }
                    else
                    {
                        pd.setTitle("Saving...");
                        pd.setMessage("Saving post...");
                        pd.setCancelable(true);
                        pd.show();
                        String imei=GetPresistenceData.getMyIMEI(getContext());
                        String email=GetPresistenceData.getEmail(getContext());
                        String name=GetPresistenceData.getName(getContext());
                        Log.e("Image_click_value",image);
                        uploadtoserverr(image,ideas_text,imei,email,name,getContext());
                    }

                }
            }
        });

        // set constraint
        //Picasso.get().load(GetPresistenceData.getProfilepic(getContext())).into(imageChange);
        //Picasso.get().load(GetPresistenceData.getProfilepic(getContext())).into(backImg);
        //userName.setText(GetPresistenceData.getName(getContext()));
        //userMob.setText(GetPresistenceData.getUsername(getContext()));

        // update profile pic
       /* changeDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChooseImageOptions();
            }
        });*/
        return view;
    }

    private void alertBox() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Change profile photo"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            ChooseImageOptions();
                        }
                    }
                });
        pictureDialog.show();
    }


    private void ChooseImageOptions() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Capture to Camera",
                "From Gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        switch (i) {
                            case 0:
                                cameraImage();
                                pd.setMessage("Uploading...");
                                break;
                            case 1:
                                dataFromLib();

                                pd.setMessage("Uploading...");
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
/*    private void parse2(String response2) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);

                    String name = jsonObject1.getString("name");

                    arr.add(name);
                    // setup auto complete view
                    adapter=new ArrayAdapter<>(getContext(),android.R.layout.select_dialog_item, arr);
                    searchEditBox.setThreshold(1);//will start working from first character
                    searchEditBox.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                    searchEditBox.setTextColor(Color.RED);
                    searchEditBox.setDropDownBackgroundResource(R.color.white);
                }
            }
            else{

                Toast.makeText(getContext(), "Network Issue", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class loadAllFriends extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            Cache cache1 = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue1=new RequestQueue(cache1,network1);
            queue1.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.all_friends, new Response.Listener<String>() {
                @Override
                public void onResponse(String response2) {
                    parse2(response2);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    mydata.put("imei", GetPresistenceData.getMyIMEI(getContext()));


                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue1.add(request);
            return null;
        }
    }*/

    public static void loadFriends(final Context mcontext, final String page, final String lastid) {

        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.suggest_frnds, new Response.Listener<String>() {
            @Override
            public void onResponse(String response3) {
                parseData3(response3,mcontext,page);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei",GetPresistenceData.getMyIMEI(mcontext));
                mydata.put("pageFrom",page);
                mydata.put("lastid",lastid);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private static void parseData3(String response3, Context mcontext, String page) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response3);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Tab1.frndRec.setVisibility(View.VISIBLE);
                Tab1.recomandedFriend.setVisibility(View.VISIBLE);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    recomandedFriend.setVisibility(View.VISIBLE);
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String imei = jsonObject1.getString("imei");
                    String total = jsonObject1.getString("total");
                    String name = jsonObject1.getString("name");
                    String my_pic = jsonObject1.getString("my_pic");
                    String email = jsonObject1.getString("email");


                    Frnds frnd2 = new Frnds(imei, name, my_pic, email, GetPresistenceData.getMyIMEI(mcontext), total);
                    Frnd.add(frnd2);

                    FrndRec2 frndRec2 = new FrndRec2(mcontext, Frnd);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(mcontext);
                    layoutManager.setOrientation(RecyclerView.HORIZONTAL);
                    frndRec.setLayoutManager(layoutManager);
                    frndRec.setHasFixedSize(true);
                    frndRec.setAdapter(frndRec2);
                    //suggestFRec.scrollToPosition(5); //use to focus the item with index
                    //rSuugested.notifyDataSetChanged();

                }
            }
            else
            {
                Tab1.frndRec.setVisibility(View.GONE);
                Tab1.recomandedFriend.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        Frnd.clear();
        //loadFriends(getContext(),"T","T");
        //pd.dismiss();
    }



    private void dataFromLib() {
        //method to show file chooser
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY2);
    }

    private void cameraImage() {
        cameravalue="1";
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }
    public static void loadPost(final Context mcontext, final String page, final String lastid) {
        pd.show();
        posts.clear();
        RequestQueue queue;
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.showPost, new Response.Listener<String>() {
            @Override
            public void onResponse(String response3) {
                parseData2(response3,mcontext,page);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei",GetPresistenceData.getMyIMEI(mcontext));
                mydata.put("pageFrom",page);
                mydata.put("lastid",lastid);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    @SuppressLint("SetTextI18n")
    private static void parseData2(String response3, Context mcontext, String page) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response3);

            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String total = jsonObject1.getString("total");
                    String sr = jsonObject1.getString("sr");
                    String imei = jsonObject1.getString("imei");
                    String post_like = jsonObject1.getString("post_like");
                    String post_comments = jsonObject1.getString("post_comments");
                    String email = jsonObject1.getString("email");
                    String date1 = jsonObject1.getString("date1");
                    String name = jsonObject1.getString("name");
                    String post_txt = jsonObject1.getString("post_txt");
                    String post_img = jsonObject1.getString("post_img");
                    String shared = jsonObject1.getString("shared");
                    String shared_name = jsonObject1.getString("shared_name");
                    String like = jsonObject1.getString("like");
                    String dp = jsonObject1.getString("dp");
                    String total_like=jsonObject1.getString("total_like");
                    pd.dismiss();

                    Posts posts2 = new Posts(sr, post_like, post_comments, imei, email, date1, name, post_txt, post_img, shared, shared_name, like, dp, total,total_like);
                    posts.add(posts2);

                    RPOST rpost = new RPOST(mcontext, posts);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(mcontext);
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    recPost.setLayoutManager(layoutManager);
                    recPost.setHasFixedSize(true);
                    recPost.setAdapter(rpost);
                }
            }
            else
            {
                pd.dismiss();
                recPost.setVisibility(View.GONE);
                recomandedText.setVisibility(View.GONE);
                alertLay.setVisibility(View.VISIBLE);
                alertText.setText("No recent posts found");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    public static void uploadtoserverr(final String image1, final String ideas_text, final String imei, final String email, final String name, final Context context) {
        Cache cache1 = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.savePost, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {

                parseData1(response1,context);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",imei);
                mydata.put("email",email);
                mydata.put("name",name);
                mydata.put("post_txt",ideas_text);
                mydata.put("post_img",image1);
                mydata.put("shared","0");
                mydata.put("shared_name","0");
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }
    private static void parseData1(String response1,Context context) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                pd.dismiss();

                Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show();
                //imgView.setImageDrawable(null);
                postLay3.setVisibility(View.GONE);
                // load posts
                posts.clear();
                //posts.remove(posts.size()-1);
                loadPost(context,"T","T");
                recPost.setVisibility(View.VISIBLE);
                recomandedText.setVisibility(View.VISIBLE);
                alertLay.setVisibility(View.GONE);


            }
            else{
                pd.dismiss();
                Toast.makeText(context, "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void constraints() {
        //postLay.setVisibility(View.GONE);
        shareIdeaText.setText("");
        shareIdeaText.requestFocus();
    }

    private void chooseImageOptions() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Capture to Camera",
                "From Gallery",
                "Post without image"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        switch (i) {
                            case 0:
                                captureimagefromcamera();
                                pd.show();
                                pd.setMessage("Uploading...");
                                break;
                            case 1:
                                gallery();
                                pd.show();
                                pd.setMessage("Uploading...");
                                break;
                            case 2:
                                postLay.setVisibility(View.VISIBLE);

                                break;

                        }
                    }
                });
        pictureDialog.show();
    }

    private void gallery() {
        if(PackageManager.PERMISSION_GRANTED !=
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }else {
//Yeah! I want both block to do the same thing, you can write your own logic, but this works for me.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }
        }else {
//Permission Granted, lets go pick photo
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // this flag is important in Mi mobiles
            // intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, GALLERY);

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA && data != null) {
//imgage captured
            if(cameravalue.equals("2")) {
                Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                assert bitmap != null;
                imageToString(bitmap);
                imgView.setImageBitmap(bitmap);
                postLay3.setVisibility(View.VISIBLE);
                imgView.setVisibility(View.VISIBLE);
                //capture_image.setImageBitmap(bitmap);
            }
            else
            {
                pd.show();
                Bitmap bitmap = (Bitmap) (Objects.requireNonNull(data.getExtras())).get("data");
                assert bitmap != null;
                imageToString3(bitmap);

                //capture_image.setImageBitmap(bitmap);
            }
        }

        else if (requestCode == GALLERY && data != null && data.getData() != null ) {
            if (resultCode == RESULT_OK) {

                Uri contentURI = data.getData();
                Log.e("Internal Uri", String.valueOf(contentURI));
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    Log.e("bitmap....", String.valueOf(bitmap));
                    imageToString2(bitmap);
                    imgView.setImageBitmap(bitmap);
                    //postLay.setVisibility(View.VISIBLE);
                    postLay3.setVisibility(View.VISIBLE);
                    imgView.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                //sendData();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Failed to select the image.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Failed to select image",
                        Toast.LENGTH_LONG).show();
            }
        }
        else if (requestCode == GALLERY2 && data != null && data.getData() != null ) {
            if (resultCode == RESULT_OK) {
                pd.show();
                Uri contentURI = data.getData();
                Log.e("Internal Uri", String.valueOf(contentURI));
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    Log.e("bitmap....", String.valueOf(bitmap));
                    imageToString4(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                //sendData();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Failed to select the image.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Failed to select image",
                        Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        pd.dismiss();
    }
    private void imageToString3(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        pd.dismiss();
        Log.e("value",image);
        if(image != null)
        {
            upload_profile_dp(image,"1");
        }
    }

    private void imageToString2(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        Log.e("base64 Conversion",image);
        pd.dismiss();
    }
    private void imageToString4(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        Log.e("base64 Conversion",image);
        pd.dismiss();

        if(image != null)
        {
            upload_profile_dp(image,"1");
        }

    }

    public static void universal(String text,Context context)
    {
       // Picasso.get().load(text).into(imageChange);
       // uploadtoserverr(text,"Update current profile image",GetPresistenceData.getMyIMEI(context),GetPresistenceData.getEmail(context),GetPresistenceData.getName(context),context);

    }

    private void upload_profile_dp(final String data, final String type) {
        Cache cache1 = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.update_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                dp_response(response1, data);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei", GetPresistenceData.getMyIMEI(Objects.requireNonNull(getContext())));
                mydata.put("email",GetPresistenceData.getEmail(getContext()));
                mydata.put("type",type);
                mydata.put("data",data);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }
    private void dp_response(String response1, String data) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String dp = jsonObject1.getString("dp");
                    pd.dismiss();
                    //new fetchProfileDate().execute("","","");
                    Toast.makeText(getContext(), "Updated profile image", Toast.LENGTH_SHORT).show();
                    PresistenceData.saveProfile(getContext(), GetPresistenceData.getMyIMEI(Objects.requireNonNull(getContext())),GetPresistenceData.getName(getContext()), GetPresistenceData.getUsername(getContext()), GetPresistenceData.getEmail(getContext()),dp,GetPresistenceData.getPassword(getContext()));
                    Picasso.get().load(dp).into(imageChange);
                    //Picasso.get().load(dp).into(backImg);
                    uploadtoserverr(dp,"Update current profile image",GetPresistenceData.getMyIMEI(getContext()),GetPresistenceData.getEmail(getContext()),GetPresistenceData.getName(getContext()),getContext());

                }
            }
            else{
                pd.dismiss();
                Toast.makeText(getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void captureimagefromcamera() {
        cameravalue="2";
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onButtonClicked(String text) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




}
