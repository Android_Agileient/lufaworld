package com.lufaword.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lufaword.Campaign.Active;
import com.lufaword.Campaign.RActive;
import com.lufaword.Configuration.Config;
import com.lufaword.Login.Register;
import com.lufaword.Login.Terms;
import com.lufaword.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.lufaword.Fragments.Tab1.queue;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab2 extends Fragment {
    private RecyclerView campaignRec;
    //private BottomNavigationView navigationView;
    private ArrayList<Active> active;
    private LinearLayout noCampLay;
    private ImageView noCampImage;
    private RequestQueue queue1;
    private static final String REQUEST_TAG = "VolleyBlockingRequestActivity";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Tab2() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab2.
     */
    // TODO: Rename and change types and number of parameters
    private static Tab2 newInstance(String param1, String param2) {
        Tab2 fragment = new Tab2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //active=new ArrayList<>();
        //active.clear();




        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.campaign, container, false);
        campaignRec = (RecyclerView) view.findViewById(R.id.campaign_rec);
        //navigationView = (BottomNavigationView) view.findViewById(R.id.navigationView);
        noCampLay = (LinearLayout) view.findViewById(R.id.no_camp_lay);
        noCampImage = (ImageView) view.findViewById(R.id.no_camp_image);




        // initilize array list
        active=new ArrayList<>();
        active.clear();
        loadActiveCampaign(getContext(),"T","T");



        return view;
    }
    private void loadActiveCampaign(final Context applicationContext, final String page, final String lastid) {

        Cache cache1 = new DiskBasedCache(Objects.requireNonNull(getActivity()).getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());
        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.active_campaign, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseData(response,applicationContext,page);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(applicationContext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("pageFrom",page);
                mydata.put("lastid",lastid);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData(String response, Context applicationContext, String page) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response);

            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String sr = jsonObject1.getString("sr");
                    String camp_sr = jsonObject1.getString("camp_sr");
                    String start_date = jsonObject1.getString("start_date");
                    String start_time = jsonObject1.getString("start_time");
                    String stop_date = jsonObject1.getString("stop_date");
                    String stop_time = jsonObject1.getString("stop_time");
                    String target_amount = jsonObject1.getString("target_amount");
                    String msg_title = jsonObject1.getString("msg_title");
                    String msg_desc = jsonObject1.getString("msg_desc");
                    String msg_image = jsonObject1.getString("msg_image");
                    String msg_video = jsonObject1.getString("msg_video");
                    String username = jsonObject1.getString("username");
                    String rec_amount = jsonObject1.getString("rec_amount");
                    String min = jsonObject1.getString("min");
                    String hr = jsonObject1.getString("hr");
                    String day = jsonObject1.getString("day");


                    Active active2 = new Active(sr, camp_sr, start_date, start_time, stop_date, stop_time, target_amount, msg_title, msg_desc, msg_image, msg_video, username, rec_amount,min,hr,day);
                    active.add(active2);

                    RActive ractive = new RActive(applicationContext, active);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(applicationContext);
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    campaignRec.setLayoutManager(layoutManager);
                    campaignRec.setHasFixedSize(true);
                    campaignRec.setAdapter(ractive);
                }
            }
            else
            {
                campaignRec.setVisibility(View.GONE);
                noCampLay.setVisibility(View.VISIBLE);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
    // backKey pressed


    @Override
    public void onStart() {
        super.onStart();
        /*active.clear();
        loadActiveCampaign(getContext(),"T","T");*/
    }
    // close all connection on stop method
    @Override
    public void onStop() {
        super.onStop();
        if(queue1 !=null)
        {
            queue1.cancelAll(REQUEST_TAG);
        }
        if(queue !=null)
        {
            queue.cancelAll(REQUEST_TAG);
        }

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
