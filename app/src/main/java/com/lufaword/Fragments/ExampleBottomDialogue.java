package com.lufaword.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.media.MediaRecorder.VideoSource.CAMERA;
import static com.lufaword.Fragments.Tab1.pd;

public class ExampleBottomDialogue extends BottomSheetDialogFragment {
    private BottomSheetListener mlistner;
    String cameravalue,image="";
    int GALLERY2 = 3;
    RequestQueue queue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       View v=inflater.inflate(R.layout.model_profile_pic,container,false);


        ImageView changeProfile1 = (ImageView) v.findViewById(R.id.change_profile_1);
        TextView changeProfile = (TextView) v.findViewById(R.id.change_profile);

        // change profile pic
        changeProfile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chooseImage();

            }
        });
        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });


        return v;
    }

    private void chooseImage() {

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Capture to Camera",
                "From Gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        switch (i) {
                            case 0:
                                cameraImage();

                                break;
                            case 1:
                                dataFromLib();

                               // pd.setMessage("Uploading...");
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    private void dataFromLib() {
        //method to show file chooser
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY2);
    }

    private void cameraImage() {
            cameravalue="1";
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
    }
    private void imageToString3(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        //pd.dismiss();
        Log.e("value",image);
        if(image == null)
        {

        }
        else
        {
            upload_profile_dp(image,"1");
        }
    }

    private void upload_profile_dp(final String data, final String type) {
        Cache cache1 = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.update_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                dp_response(response1, data);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei", GetPresistenceData.getMyIMEI(getContext()));
                mydata.put("email",GetPresistenceData.getEmail(getContext()));
                mydata.put("type",type);
                mydata.put("data",data);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void dp_response(String response1, String data) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String dp = jsonObject1.getString("dp");
                    //pd.dismiss();
                    //new fetchProfileDate().execute("","","");
                    Toast.makeText(getContext(), "Updated profile image", Toast.LENGTH_SHORT).show();
                    PresistenceData.saveProfile(getContext(), GetPresistenceData.getMyIMEI(getContext()),GetPresistenceData.getName(getContext()), GetPresistenceData.getUsername(getContext()), GetPresistenceData.getEmail(getContext()),dp,GetPresistenceData.getPassword(getContext()));
                    mlistner.onButtonClicked(dp);

                }
            }
            else{
                pd.dismiss();
                Toast.makeText(getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void imageToString4(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        Log.e("base64 Conversion",image);
        //pd.dismiss();
        if(image == null)
        {

        }
        else
        {
            upload_profile_dp(image,"1");
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA && data != null) {
//imgage captured
            if(cameravalue.equals("1")) {
                //pd.show();
                Bitmap bitmap = (Bitmap) (Objects.requireNonNull(data.getExtras())).get("data");
                assert bitmap != null;
                imageToString3(bitmap);

                //capture_image.setImageBitmap(bitmap);
            }

        }


        else if (requestCode == GALLERY2 && data != null && data.getData() != null ) {
            if (resultCode == RESULT_OK) {
                //pd.show();
                Uri contentURI = data.getData();
                Log.e("Internal Uri", String.valueOf(contentURI));
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    Log.e("bitmap....", String.valueOf(bitmap));
                    imageToString4(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                //sendData();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Failed to select the image.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Failed to select image",
                        Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public interface BottomSheetListener{
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mlistner = (BottomSheetListener) context;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString() +"must implement BottomSheetListner");
        }

    }
}
