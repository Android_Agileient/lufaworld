package com.lufaword.Campaign;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeInitializationResult;

import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.lufaword.BuildConfig;
import com.lufaword.Configuration.Config;
import com.lufaword.MainActivity;
import com.lufaword.NetworkNot;
import com.lufaword.Payments.PayMain;
import com.lufaword.Payments.Paymain2;
import com.lufaword.R;
import com.lufaword.Utils.AppStatus;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Objects;

public class RActive extends RecyclerView.Adapter<RActive.Holder>{

    private ArrayList<Active>active;
    private Context mcontext;
    private String Url,min,hr,day;
    private int w,minn;
    public RActive(Context mcontext, ArrayList<Active> active) {
        this.active=active;
        this.mcontext=mcontext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.active_campaign,parent,false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        String sr=active.get(position).getSr();
        String camp_sr=active.get(position).getCamp_sr();
        String start_date=active.get(position).getStart_date();
        String start_time=active.get(position).getStart_time();
        String stop_date=active.get(position).getStop_date();
        String stop_time=active.get(position).getStop_time();
        String target_amount=active.get(position).getTarget_amount();
        String msg_title=active.get(position).getMsg_title();
        String msg_desc=active.get(position).getMsg_desc();
        final String msg_image=active.get(position).getMsg_image();
        final String msg_video=active.get(position).getMsg_video();
        String username=active.get(position).getUsername();
        String rec_amount=active.get(position).getRec_amount();
        min=active.get(position).getMin();
        hr=active.get(position).getHr();
        day=active.get(position).getDay();

        // set Parameters
        holder.healine.setText(msg_title);
        holder.desc.setText(msg_desc);
        Picasso.get().load(msg_image).into(holder.imageView);
        holder.achieveText.setText("$"+rec_amount+" raised of $"+target_amount);
        holder.donateProgress.setMax(Integer.parseInt(target_amount));
        holder.donateProgress.setProgress(Integer.parseInt(rec_amount));
        holder.noFees.setText("No Fees");
        // time and date
        if(!day.equals("0"))
        {
            if(!hr.equals("0")) {
                holder.dayLeft.setText(day + " Days : " + hr + " Hours : " + min + " Min. Left");
            }
            else
            {
                holder.dayLeft.setText(min+" Min. Left");
            }
        }
        else
        {
            holder.dayLeft.setText(hr+" Hours : "+min+" Min. Left");
        }
        // start handler
        timer(holder,Integer.parseInt(min),mcontext);

        // set Parameters of Image view only and only when we will receive the url of image / in case of NA visibility gone
        if(msg_image.equals("NA"))
        {
            holder.imageView.setVisibility(View.GONE);
        }
        else
        {
            holder.imageView.setVisibility(View.VISIBLE);
        }

        // set video id if video id is not available then we will receive NA and then hide visibility of Thumnail
        if(msg_video.equals("NA"))
        {
            holder.youtubeImage.setVisibility(View.GONE);
            holder.playIcon.setVisibility(View.GONE);
        }
        else {
            String image_url="https://img.youtube.com/vi/"+msg_video+"/0.jpg";
            Picasso.get().load(image_url).into(holder.youtubeImage);
            holder.youtubeImage.setVisibility(View.VISIBLE);
            holder.playIcon.setVisibility(View.VISIBLE);
          /*  holder.youtubeImage.setVisibility(View.VISIBLE);
            holder.playIcon.setVisibility(View.VISIBLE);
            final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                @Override
                public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                }

                @Override
                public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                    youTubeThumbnailView.setVisibility(View.VISIBLE);

                }
            };

            holder.youtubeImage.initialize(Config.API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                    youTubeThumbnailLoader.setVideo(msg_video);
                    youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
                    //youTubeThumbnailLoader.release();

                }

                @Override
                public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                    //write something for failure
                }
            });*/
        }
        holder.playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext,VideoPlayer.class);
                intent.putExtra("video_id",msg_video);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(intent);
            }
        });


// click on thumnail and start video
/*        holder.youtubeThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) mcontext, API_KEY, "A0YB8jpGpqQ");
                mcontext.startActivity(intent);*//*
                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) mcontext,
                        API_KEY,
                        "A0YB8jpGpqQ",//video id
                        100,     //after this time, video will start automatically
                        true,               //autoplay or not
                        false);             //lightbox mode or not; show the video in a small box
                mcontext.startActivity(intent);
            }
        });*/
        // lcik on donate now page
        holder.donateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // save data in presistence storage
                PresistenceData.ActiveCamp(mcontext,active.get(position).getSr());
                Intent intent=new Intent(mcontext, Paymain2.class);
                mcontext.startActivity(intent);
            }
        });

        // click on share
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(active.get(position).getMsg_image().equals("NA"))
                {
                    Intent intent=new Intent(Intent.ACTION_SEND);
                    intent.setType("text/*");
                    //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                    String data = "Lufa World \n\n "+active.get(position).getMsg_title()+"\n\n"+active.get(position).getMsg_desc()+" \n \n***********\n "+"Donation Target $"+active.get(position).getTarget_amount()+"\n"+"Achieved Target $"+active.get(position).getRec_amount()+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName());
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_TEXT,data);
                    intent.setPackage(Config.whatsapp_package);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // for particular choose we will set getPackage()
                    /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
                    mcontext.startActivity(intent);
                }
                else
                {
                    PackageManager manager = mcontext.getPackageManager();
                    Picasso.get().load(msg_image).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            /* uri1=Uri.parse(Paths+File.separator+"10.jpg");*/
                            Intent intent=new Intent(Intent.ACTION_SEND);
                            intent.setType("image/*");
                            //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                            String data = "Lufa World \n\n "+active.get(position).getMsg_title()+"\n\n"+active.get(position).getMsg_desc()+" \n \n***********\n "+"Donation Target $"+active.get(position).getTarget_amount()+"\n"+"Achieved Target $"+active.get(position).getRec_amount()+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName());
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.putExtra(Intent.EXTRA_TEXT,data);
                            intent.putExtra(Intent.EXTRA_STREAM,getLocalBitmapUri(bitmap,mcontext));
                            intent.setPackage(Config.whatsapp_package);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            // for particular choose we will set getPackage()
                            /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
                            mcontext.startActivity(intent);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            Toast.makeText(mcontext, "Load Image Failed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

                    // end Share code
                }
            }
        });

    }

    private Uri getLocalBitmapUri(Bitmap bmp, Context context) {

        Uri uriimg = null;

        try {

            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LufaWorld" + System.currentTimeMillis() + ".png");

            FileOutputStream out = new FileOutputStream(file);

            bmp.compress(Bitmap.CompressFormat.PNG, 50, out);

            out.close();

            /*uriimg = Uri.fromFile(file);*/
            uriimg = FileProvider.getUriForFile(Objects.requireNonNull(mcontext),
                    BuildConfig.APPLICATION_ID + ".provider", file);
            refreshGallery(file);
        } catch (IOException e) {

            e.printStackTrace();

        }

        return uriimg;

    }

    private void refreshGallery(File file) {
        Intent intent1 = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent1.setData(Uri.fromFile(file));
        Objects.requireNonNull(mcontext).sendBroadcast(intent1);
    }

    private void timer(final Holder holder,final Integer min,final Context mcontext) {
        minn=min;
        (new Thread(new Runnable()
        {

            @Override
            public void run()
            {

                while (w<= minn)
                    try
                    {

                        Thread.sleep(60000);
                        ((Activity)mcontext).runOnUiThread(new Runnable() // start actions in UI thread
                        {

                            @Override
                            public void run()
                            {
                               minn=minn-1;

                                if(!day.equals("0"))
                                {
                                    if(!hr.equals("0")) {
                                        holder.dayLeft.setText(day + " Days : " + hr + " Hours : " + minn + " Min. Left");
                                    }
                                    else
                                    {
                                        holder.dayLeft.setText(minn+" Min. Left");
                                    }
                                }
                                else
                                {
                                    holder.dayLeft.setText(hr+" Hours : "+minn+" Min. Left");
                                }


                                w++;

                            }
                        });
                    }
                    catch (InterruptedException e)
                    {
                        // ooops
                    }
            }
        })).start();
    }

    @Override
    public int getItemCount() {
        return active.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView healine;
        private TextView desc;
        private ImageView share;
        private TextView shareTxt;
        private ImageView targetBackImage;
        private TextView helpingLine;
        private ContentLoadingProgressBar donateProgress;
        private TextView achieveText;
        private Button donateBtn;
        private TextView noFees;
        private ImageView youtubeImage;
        private ImageView playIcon;
        private TextView dayLeft;






        Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            healine = (TextView) itemView.findViewById(R.id.healine);
            desc = (TextView) itemView.findViewById(R.id.desc);
            share = (ImageView) itemView.findViewById(R.id.share);
            shareTxt = (TextView) itemView.findViewById(R.id.share_txt);
            targetBackImage = (ImageView) itemView.findViewById(R.id.target_back_image);
            helpingLine = (TextView) itemView.findViewById(R.id.helping_line);
            donateProgress = (ContentLoadingProgressBar) itemView.findViewById(R.id.donate_progress);
            achieveText = (TextView) itemView.findViewById(R.id.achieve_text);
            donateBtn = (Button) itemView.findViewById(R.id.donate_btn);
            noFees = (TextView) itemView.findViewById(R.id.no_fees);
            youtubeImage = (ImageView) itemView.findViewById(R.id.youtube_image);
            playIcon = (ImageView) itemView.findViewById(R.id.play_icon);
            dayLeft = (TextView) itemView.findViewById(R.id.day_left);





        }
    }
}
