package com.lufaword.Campaign;

public class Active {
    private String sr,camp_sr,start_date,start_time,stop_date,stop_time,target_amount,msg_title,msg_desc,msg_image,msg_video,username,rec_amount,min,hr,day;
    public Active(String sr, String camp_sr, String start_date, String start_time, String stop_date, String stop_time, String target_amount, String msg_title, String msg_desc, String msg_image, String msg_video, String username, String rec_amount,String min,String hr,String day) {
    this.sr=sr;
    this.camp_sr=camp_sr;
    this.start_date=start_date;
    this.start_time=start_time;
    this.stop_date=stop_date;
    this.stop_time=stop_time;
    this.target_amount=target_amount;
    this.msg_title=msg_title;
    this.msg_desc=msg_desc;
    this.msg_image=msg_image;
    this.msg_video=msg_video;
    this.username=username;
    this.rec_amount=rec_amount;
    this.min=min;
    this.hr=hr;
    this.day=day;
    }
    //getter and setter method

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getCamp_sr() {
        return camp_sr;
    }

    public void setCamp_sr(String camp_sr) {
        this.camp_sr = camp_sr;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStop_date() {
        return stop_date;
    }

    public void setStop_date(String stop_date) {
        this.stop_date = stop_date;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getTarget_amount() {
        return target_amount;
    }

    public void setTarget_amount(String target_amount) {
        this.target_amount = target_amount;
    }

    public String getMsg_title() {
        return msg_title;
    }

    public void setMsg_title(String msg_title) {
        this.msg_title = msg_title;
    }

    public String getMsg_desc() {
        return msg_desc;
    }

    public void setMsg_desc(String msg_desc) {
        this.msg_desc = msg_desc;
    }

    public String getMsg_image() {
        return msg_image;
    }

    public void setMsg_image(String msg_image) {
        this.msg_image = msg_image;
    }

    public String getMsg_video() {
        return msg_video;
    }

    public void setMsg_video(String msg_video) {
        this.msg_video = msg_video;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRec_amount() {
        return rec_amount;
    }

    public void setRec_amount(String rec_amount) {
        this.rec_amount = rec_amount;
    }
}
