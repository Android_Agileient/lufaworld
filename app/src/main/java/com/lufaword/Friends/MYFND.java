package com.lufaword.Friends;

class MYFND {
    String name,imei,pic;
    MYFND(String name, String imei, String pic) {
        this.name=name;
        this.imei=imei;
        this.pic=pic;
    }
    // getter and setter method

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
