package com.lufaword.Friends;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Async.saveNotification;
import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.R;
import com.lufaword.Utils.AppStatus;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FrndRec2 extends RecyclerView.Adapter<FrndRec2.Holder> {
    ArrayList<Frnds> frnd;
    Context mcontext;
    String name;
    RequestQueue queue;
    public FrndRec2(Context mcontext, ArrayList<Frnds> frnd) {
        this.frnd=frnd;
        this.mcontext=mcontext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.suugest_me,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
String dp=frnd.get(position).getMy_pic();
        Picasso.get().load(dp).into(holder.img);
        holder.name.setText(frnd.get(position).getName());
        name=frnd.get(position).getName();
        String layPos=String.valueOf(holder.getLayoutPosition());
        String arraySizee=String.valueOf(frnd.size());

        // if list scroll to the last element then load new data
        if(layPos.equals(String.valueOf(Integer.parseInt(arraySizee)-1)))
        {
            Tab1.loadFriends(mcontext,String.valueOf(Integer.parseInt(layPos) + 10),frnd.get(position).getImei());
           /* if (AppStatus.getInstance(mcontext).isOnline()) {
                if(arraySizee.equals(String.valueOf(Integer.parseInt(frnd.get(position).getTotal()))))
                {
                    // max fetch value ends on ASC first sr which match to imei
                }
                else {

                    Tab1.loadFriends(mcontext,String.valueOf(Integer.parseInt(layPos) + 10),frnd.get(position).getImei());
                }

            }*/
        }



        // add Friend
        holder.addFrndBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    addFriends(frnd.get(position).myIMEI,frnd.get(position).getImei(),holder);
            }
        });

    }

    private void addFriends(final String myimei, final String frnd_imei, final Holder holder) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.add_friend, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                saveRes(response1,holder,frnd_imei);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",myimei);
                mydata.put("frnd_imei",frnd_imei);


                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void saveRes(String response1, Holder holder,String frnd_imei) {
            JSONObject jsonObject = null;

            try {
                jsonObject = new JSONObject(response1);
                String Status=jsonObject.getString("status");
                if(Status.equals("1")) {
                    Toast.makeText(mcontext,"You added "+name, Toast.LENGTH_SHORT).show();
                    frnd.remove(holder.getLayoutPosition());
                    notifyItemRemoved(holder.getLayoutPosition());
                    notifyItemRangeChanged(holder.getLayoutPosition(), frnd.size());
                    new saveNotification(frnd_imei,GetPresistenceData.getName(mcontext),",Added you as a friend ","1",mcontext,"f_req").execute();
                    // if size of array list is null then we will call the method again
                    if(frnd.size()==0)
                    {

                        Tab1.frndRec.setVisibility(View.GONE);
                        Tab1.recomandedFriend.setVisibility(View.GONE);
                    }
                }
                else{
                    Toast.makeText(mcontext, "We think you are not in network", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }


    @Override
    public int getItemCount() {
        return frnd.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView name;
        private Button addFrndBtn;



        Holder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            name = (TextView) itemView.findViewById(R.id.name);
            addFrndBtn = (Button) itemView.findViewById(R.id.add_frnd_btn);
        }
    }

}
