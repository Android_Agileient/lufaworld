package com.lufaword.Friends;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.tabs.TabItem;
import com.lufaword.Async.saveNotification;
import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

class FSearchRec extends RecyclerView.Adapter<FSearchRec.Holder> {
    private Context mcontext;
    private ArrayList<SFrnd>sFrnd;
    RequestQueue queue,queue1;

    FSearchRec(Context mcontext, ArrayList<SFrnd> sfrnd) {
        this.mcontext=mcontext;
        this.sFrnd=sfrnd;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.search_frnd_lay,parent,false);
        return new FSearchRec.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        String frnd=sFrnd.get(position).getFrnd();
        String profile_image=sFrnd.get(position).getUser_pic();


        // set Constraints
        Picasso.get().load(profile_image).into(holder.profileImage);
        holder.username.setText(sFrnd.get(position).getName());
        if(sFrnd.get(position).getFrnd().equals("1"))
        {
            holder.hint.setText("You and "+sFrnd.get(position).getName()+" are Friend");
            holder.removeFrnd.setVisibility(View.VISIBLE);
            holder.addFriend.setVisibility(View.GONE);
        }
        else
        {
            holder.hint.setText("You can add "+sFrnd.get(position).getName()+" as Friend");
            holder.addFriend.setVisibility(View.VISIBLE);
            holder.removeFrnd.setVisibility(View.GONE);
        }

        // remove friend from the list and send the message to the chat
        holder.removeFrnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                removeFrnd(sFrnd.get(position).getUser_imei(), GetPresistenceData.getMyIMEI(mcontext),holder);
            }
        });
        // Add friend from the list and send the notification to the user
        holder.addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriends(GetPresistenceData.getMyIMEI(mcontext),sFrnd.get(position).getUser_imei(),holder);
            }
        });
        // hide the user by click on close it button
        holder.deleteFromList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sFrnd.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), sFrnd.size());
                if(sFrnd.size()==0)
                {
                   // MainActivity.viewPager2.setCurrentItem(0);
                    //((Activity)mcontext).finish();


                    FragmentManager manager = ((AppCompatActivity)mcontext).getSupportFragmentManager();
                    Tab1 tab1 = new Tab1();
                    manager.beginTransaction().replace(R.id.container,tab1)
                            .commit();

                }

            }
        });

    } // onBind closer

    private void addFriends(final String myIMEI, final String user_imei, final Holder holder) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.add_friend, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                saveRes(response1,holder,user_imei);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",myIMEI);
                mydata.put("frnd_imei",user_imei);


                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void removeFrnd(final String user_imei, final String myIMEI, final Holder holder) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1 = new RequestQueue(cache1, network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.remove_frnd, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData1(response1,holder,user_imei);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei", myIMEI);
                mydata.put("frnd_imei",user_imei);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }
    private void saveRes(String response1, Holder holder, String user_imei) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "Friend Added", Toast.LENGTH_SHORT).show();
               /* sFrnd.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), sFrnd.size());*/
                new saveNotification(user_imei,GetPresistenceData.getName(mcontext),",Added you as a friend ","1",mcontext,"f_req").execute();
                holder.addFriend.setVisibility(View.GONE);
                holder.removeFrnd.setVisibility(View.VISIBLE);


            }
            else{
                Toast.makeText(mcontext, "We think you are not in network", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void parseData1(String response1, Holder holder,String user_imei) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "Removed from friendlist", Toast.LENGTH_SHORT).show();
               /* sFrnd.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), sFrnd.size());*/
                new saveNotification(user_imei,GetPresistenceData.getName(mcontext),",Removed you from friend list","1",mcontext,"f_rem").execute();
                holder.addFriend.setVisibility(View.VISIBLE);
                holder.removeFrnd.setVisibility(View.GONE);
            }
            else
            {
                Toast.makeText(mcontext, "Unable to remove", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    @Override
    public int getItemCount() {
        return sFrnd.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private CircleImageView profileImage;
        private TextView username;
        private Button addFriend;
        private Button removeFrnd;
        private TextView hint;
        private ImageView deleteFromList;








        Holder(@NonNull View itemView) {
            super(itemView);
            profileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);
            username = (TextView) itemView.findViewById(R.id.username);
            addFriend = (Button) itemView.findViewById(R.id.add_friend);
            removeFrnd = (Button) itemView.findViewById(R.id.remove_frnd);
            hint = (TextView) itemView.findViewById(R.id.hint);
            deleteFromList = (ImageView) itemView.findViewById(R.id.delete_from_list);
        }
    }


}
