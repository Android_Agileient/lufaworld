package com.lufaword.Friends;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Async.saveNotification;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

class MyfrndRec extends RecyclerView.Adapter<MyfrndRec.Holder> {
    ArrayList<MYFND>myfnd;
    Context mcontext;
    MyfrndRec(Context mconext, ArrayList<MYFND> myfnd) {
        this.myfnd=myfnd;
        this.mcontext=mconext;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.my_frnd_list,parent,false);
        return new MyfrndRec.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyfrndRec.Holder holder, final int position) {
        Picasso.get().load(myfnd.get(position).getPic()).into(holder.imageView);
        holder.frndName.setText(myfnd.get(position).getName());
        // click on Remove Frnd
        holder.removeFrnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeFrnd(myfnd.get(position).getImei(), GetPresistenceData.getMyIMEI(mcontext),holder);
            }
        });

    }

    private void removeFrnd(final String imei, final String myIMEI, final Holder holder) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());
        RequestQueue queue1;
        queue1 = new RequestQueue(cache1, network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.remove_frnd, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData1(response1,holder,imei);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei", myIMEI);
                mydata.put("frnd_imei",imei);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData1(String response1, Holder holder, String imei) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            if(Status.equals("1")) {

                myfnd.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), myfnd.size());
                new saveNotification(imei,GetPresistenceData.getName(mcontext),",Removed you from friend list","1",mcontext,"f_rem").execute();

            }
            else
            {
                Toast.makeText(mcontext, "Unable to remove", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


    @Override
    public int getItemCount() {
        return myfnd.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private CircleImageView imageView;
        private TextView frndName;
        private ImageView removeFrnd;



        Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (CircleImageView) itemView.findViewById(R.id.imageView);
            frndName = (TextView) itemView.findViewById(R.id.frnd_name);
            removeFrnd = (ImageView) itemView.findViewById(R.id.chat_btn);
        }
    }
}
