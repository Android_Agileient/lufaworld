package com.lufaword.Friends;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.Login.Register;
import com.lufaword.Login.Terms;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyFriends extends AppCompatActivity {
    private RecyclerView myfrndRec;
    private SwipeRefreshLayout swipe;
    ArrayList<MYFND>myfnd;
    private ImageView backArrow;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_friends);
        myfrndRec = (RecyclerView) findViewById(R.id.myfrndRec);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        myfnd=new ArrayList<>();
        myfnd.clear();

        // click on swipe refresh
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // load my friends
                myfnd.clear();
                new loadMyFriends().execute();
                swipe.setRefreshing(false);
            }
        });
      // load my friends
        new loadMyFriends().execute();

        // click on backArray to go back
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back1Intent=new Intent(MyFriends.this, MainActivity.class);
                startActivity(back1Intent);
                finish();
            }
        });


    }//onCreate closer


    @SuppressLint("StaticFieldLeak")
    private class loadMyFriends extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            RequestQueue queue2;
            Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue2=new RequestQueue(cache1,network1);
            queue2.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.my_friends, new Response.Listener<String>() {
                @Override
                public void onResponse(String response4) {
                    loadFrnds(response4);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    // no need to pass imei but as security i passed it
                    mydata.put("imei",GetPresistenceData.getMyIMEI(getApplicationContext()));

                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue2.add(request);
            return null;
        }
    }

    private void loadFrnds(String response4) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response4);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String name = jsonObject1.getString("name");
                    String imei = jsonObject1.getString("imei");
                    String pic = jsonObject1.getString("pic");

                    MYFND myfnd2 = new MYFND(name,imei,pic);
                    myfnd.add(myfnd2);

                    MyfrndRec myfrndRec1 = new MyfrndRec(getApplicationContext(), myfnd);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    myfrndRec.setLayoutManager(layoutManager);
                    myfrndRec.setHasFixedSize(true);
                    myfrndRec.setAdapter(myfrndRec1);
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "No group Found", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    // backKey pressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode==KeyEvent.KEYCODE_BACK))
        {
            Intent back1Intent=new Intent(MyFriends.this, MainActivity.class);
            startActivity(back1Intent);
            overridePendingTransition(R.anim.enter,R.anim.exit);
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }
}
