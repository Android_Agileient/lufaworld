package com.lufaword.Friends;

class SFrnd {
    String user_imei,name,email,user_pic,username,frnd;
    public SFrnd(String user_imei, String name, String email, String user_pic, String username, String frnd) {
        this.user_imei=user_imei;
        this.name=name;
        this.email=email;
        this.user_pic=user_pic;
        this.username=username;
        this.frnd=frnd;
    }
    // getter and setter method

    public String getUser_imei() {
        return user_imei;
    }

    public void setUser_imei(String user_imei) {
        this.user_imei = user_imei;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFrnd() {
        return frnd;
    }

    public void setFrnd(String frnd) {
        this.frnd = frnd;
    }
}
