package com.lufaword.Friends;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.tabs.TabItem;
import com.lufaword.Configuration.Config;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Social.Comments;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchFrnd extends AppCompatActivity {
    private RecyclerView searchFriendRec;
String frnd_name;
RequestQueue queue;
ArrayList<SFrnd> sfrnd;
private TextView heading;
ProgressDialog pd;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_frnd);
        searchFriendRec = (RecyclerView) findViewById(R.id.searchFriendRec);
        heading = (TextView) findViewById(R.id.heading);
        pd=new ProgressDialog(this);
        pd.setTitle("Searching Peoples");
        pd.setMessage("Please wait...");
        pd.setCancelable(true);
        pd.show();


        // receive intents
        Intent intent=getIntent();
        frnd_name=intent.getStringExtra("frnd_name");

        // initilize array
        sfrnd=new ArrayList<>();
        sfrnd.clear();

        // fetch searched friends
        new FriendList(frnd_name).execute();



    } //onCreate closer

    // backKey pressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode==KeyEvent.KEYCODE_BACK))
        {
            MainActivity.viewPager2.setCurrentItem(0);
            overridePendingTransition(R.anim.enter,R.anim.exit);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @SuppressLint("StaticFieldLeak")
    public class FriendList extends AsyncTask<String,String,String> {
        String frnd_name;
        FriendList(String frnd_name) {
            this.frnd_name=frnd_name;
        }

        @Override
        protected String doInBackground(String... strings) {
            Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue = new RequestQueue(cache1, network1);
            queue.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.frnd_list, new Response.Listener<String>() {
                @Override
                public void onResponse(String response1) {
                    parseData1(response1);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SearchFrnd.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    // no need to pass imei but as security i passed it
                    mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));
                    mydata.put("user_name",frnd_name);
                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue.add(request);
            return null;
        }
    }

    private void parseData1(String response1) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String user_imei = jsonObject1.getString("user_imei");
                    String name = jsonObject1.getString("name");
                    String email = jsonObject1.getString("email");
                    String user_pic = jsonObject1.getString("user_pic");
                    String username = jsonObject1.getString("username");
                    String frnd = jsonObject1.getString("frnd");

                    SFrnd sfrnd2 = new SFrnd(user_imei,name,email,user_pic,username,frnd);
                    sfrnd.add(sfrnd2);

                    FSearchRec fSearchRec = new FSearchRec(getApplicationContext(), sfrnd);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    searchFriendRec.setLayoutManager(layoutManager);
                    searchFriendRec.setHasFixedSize(true);
                    searchFriendRec.setAdapter(fSearchRec);
                    pd.dismiss();
                }
            }
            else
            {
                pd.dismiss();
                Toast.makeText(this, "No user available with this name", Toast.LENGTH_SHORT).show();
                MainActivity.viewPager2.setCurrentItem(0);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


}
