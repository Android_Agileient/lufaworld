package com.lufaword.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.lufaword.Login.ResetPass;
import com.lufaword.Login.ResetUsername;
import com.lufaword.Payments.Paymain2;
import com.lufaword.R;
import com.lufaword.Report.Transactions;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainProfile extends AppCompatActivity {
    private ImageView GoBack;
    private ImageView backImg;
    private CircleImageView imageChange;
    private ImageView editPic;
    private TextView userName;
    private TextView userEmail;
    private Button resetPass;
    private Button resetUsername;
    private Button myDonation;
    private Button newDonation;
    String image,Name,uname;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile);
        GoBack = (ImageView) findViewById(R.id.GoBack);
        backImg = (ImageView) findViewById(R.id.backImg);
        imageChange = (CircleImageView) findViewById(R.id.imageChange);
        editPic = (ImageView) findViewById(R.id.edit_pic);
        userName = (TextView) findViewById(R.id.user_name);
        userEmail = (TextView) findViewById(R.id.user_email);
        resetPass = (Button) findViewById(R.id.reset_pass);
        resetUsername = (Button) findViewById(R.id.reset_username);
        myDonation = (Button) findViewById(R.id.my_donation);
        newDonation = (Button) findViewById(R.id.new_donation);

        // get data from presistence
        image= GetPresistenceData.getProfilepic(getApplicationContext());
        Name= GetPresistenceData.getName(getApplicationContext());
        uname= GetPresistenceData.getUsername(getApplicationContext());


        // set constraints
        Picasso.get().load(image).into(imageChange);
        // animation image
        Glide.with(getApplicationContext()).asGif().load(R.drawable.circles).into(backImg);
        backImg.setVisibility(View.VISIBLE);
        userName.setText(Name);
        userEmail.setText(uname);

        // seton click on reset password
        resetUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainProfile.this, ResetUsername.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        // click on reset password
        resetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainProfile.this, ResetPass.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        // donation history click listner
        myDonation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainProfile.this, Transactions.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        // click on new donation
        newDonation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // save data in presistence storage
                // if someone donation then default given 0
                PresistenceData.ActiveCamp(getApplicationContext(),"0");
                Intent intent=new Intent(MainProfile.this, Paymain2.class);
                startActivity(intent);
            }
        });

    } //onCreate closer
}
