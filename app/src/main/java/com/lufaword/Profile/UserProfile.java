package com.lufaword.Profile;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfile extends AppCompatActivity {
    private FrameLayout profileLay;
    private CircleImageView profileCircleImage;
    private TextView noOfPosts;
    private TextView noOfFrnds;
    private TextView noOfLikes;
    private TextView profileName;
    private TextView userEmailId;
    private ImageView closeMe3;
    private LinearLayout addFriend;
    private LinearLayout removeFriends;
    private LinearLayout likeme;
    private LinearLayout unlike;
    String image,type,imei,username,email;
    RequestQueue queue;
    ProgressDialog pd;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        profileLay = (FrameLayout) findViewById(R.id.profile_lay);
        profileCircleImage = (CircleImageView) findViewById(R.id.profile_circle_image);
        noOfPosts = (TextView) findViewById(R.id.no_of_posts);
        noOfFrnds = (TextView) findViewById(R.id.no_of_frnds);
        noOfLikes = (TextView) findViewById(R.id.no_of_likes);
        profileName = (TextView) findViewById(R.id.profile_name);
        userEmailId = (TextView) findViewById(R.id.user_email_id);
        closeMe3 = (ImageView) findViewById(R.id.close_me3);
        addFriend = (LinearLayout) findViewById(R.id.add_friend);
        removeFriends = (LinearLayout) findViewById(R.id.remove_friends);
        likeme = (LinearLayout) findViewById(R.id.likeme);
        unlike = (LinearLayout) findViewById(R.id.unlike);

        pd=new ProgressDialog(this);
        pd.setTitle("Loading Profile");
        pd.setMessage("Wait...");

        // Receieve intents
        Intent intent=getIntent();
        image=intent.getStringExtra("image");
        type=intent.getStringExtra("type");
        imei=intent.getStringExtra("imei");
        username=intent.getStringExtra("username");
        email=intent.getStringExtra("email");

        // set constraints
        Picasso.get().load(image).into(profileCircleImage);
        profileName.setText(username);

        pd.show();
        new loadFriendProfile(imei, Config.frnd_profile_details).execute();

        addFriend.setVisibility(View.GONE);
        removeFriends.setVisibility(View.GONE);
        likeme.setVisibility(View.GONE);
        unlike.setVisibility(View.GONE);
        userEmailId.setText(email);


// click on add friend
        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    callAllinOneAPI("1", ", Added you as a friend");

            }
        });
        // remove friends
        removeFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    callAllinOneAPI("2", ", Removed you from friend list");

            }
        });
        // like friend
        likeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAllinOneAPI("3", ", , Like your profile picture");
            }
        });
        // unlike profile pic
        unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAllinOneAPI("4", ", unlike your profile picture");
            }
        });


    } //onCreate closer

    private void callAllinOneAPI(final String ty,final String msg) {
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue = new RequestQueue(cache1, network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST,Config.add_friends, new Response.Listener<String>() {
            @Override
            public void onResponse(String response5) {
                parseData1(response5,ty);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UserProfile.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));
                mydata.put("frnd_imei", imei);
                mydata.put("type", ty);
                mydata.put("msgg", msg);
                mydata.put("name", GetPresistenceData.getName(getApplicationContext()));
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData1(String response5,String ty) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response5);
            String Status = jsonObject.getString("status");
            if(Status.equals("1"))
            {
                // check type and as per type take action
                if(ty.equals("1"))
                {

                    Toast.makeText(this, "Friend Added", Toast.LENGTH_SHORT).show();

                    addFriend.setVisibility(View.GONE);
                    removeFriends.setVisibility(View.VISIBLE);

                }
                else if(ty.equals("2"))
                {
                    Toast.makeText(this, "Friend Removed", Toast.LENGTH_SHORT).show();

                    removeFriends.setVisibility(View.GONE);
                    addFriend.setVisibility(View.VISIBLE);
                }
                else if(ty.equals("3")) {
                    Toast.makeText(this, "Liked", Toast.LENGTH_SHORT).show();
                    likeme.setVisibility(View.GONE);
                    unlike.setVisibility(View.VISIBLE);
                }
                else if(ty.equals("4")) {
                    Toast.makeText(this, "Unliked", Toast.LENGTH_SHORT).show();
                    unlike.setVisibility(View.GONE);
                    likeme.setVisibility(View.VISIBLE);
                }

            }
            else
            {
                Toast.makeText(this, "Network Connectivity issue", Toast.LENGTH_SHORT).show();
            }

            }


         catch (JSONException e) {
            e.printStackTrace();
        };
    }

    @SuppressLint("StaticFieldLeak")
    private class loadFriendProfile extends AsyncTask<String,String,String> {
        String imei,frnd_profile_details;
        loadFriendProfile(String imei, String frnd_profile_details) {
            this.imei=imei;
            this.frnd_profile_details=frnd_profile_details;
        }

        @Override
        protected String doInBackground(String... strings) {
            Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue = new RequestQueue(cache1, network1);
            queue.start();
            StringRequest request = new StringRequest(Request.Method.POST, frnd_profile_details, new Response.Listener<String>() {
                @Override
                public void onResponse(String response5) {
                    parseData5(response5);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(UserProfile.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    mydata.put("imei", GetPresistenceData.getMyIMEI(getApplicationContext()));
                    mydata.put("frnd_imei", imei);
                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue.add(request);
            return null;
        }
    }

    private void parseData5(String response5) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response5);
            String Status = jsonObject.getString("status");
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                String frnd = jsonObject1.getString("frnd");
                String like = jsonObject1.getString("like");
                String posts = jsonObject1.getString("posts");
                String my_frnd = jsonObject1.getString("my_frnd");
                String my_like = jsonObject1.getString("my_like");

                // set post friend and like widget value
                noOfPosts.setText(posts);
                noOfFrnds.setText(frnd);
                noOfLikes.setText(like);
                pd.dismiss();
                // if my frnd value gone zero then buttons for add/remove frnds became visible
                if(!GetPresistenceData.getMyIMEI(getApplicationContext()).equals(imei)) {



                if(my_frnd.equals("0"))
                {

                    addFriend.setVisibility(View.VISIBLE);
                }
                else
                {
                    removeFriends.setVisibility(View.VISIBLE);
                }
                // if the my like value gone then respective like/unlike buttons gone visible
                if(my_like.equals("0"))
                {
                    likeme.setVisibility(View.VISIBLE);
                }
                else
                {
                    unlike.setVisibility(View.VISIBLE);
                }

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
