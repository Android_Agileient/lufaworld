package com.lufaword.Social;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Async.saveNotification;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

class CmtRec extends RecyclerView.Adapter<CmtRec.Holder> {
    ArrayList<Comment2> comments;
    Context mcontext;
    RequestQueue queue;
    CmtRec(Context mcontext, ArrayList<Comment2> comments) {
        this.mcontext=mcontext;
        this.comments=comments;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.comments,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CmtRec.Holder holder, final int position) {
        String dp=comments.get(position).getProfile_pic();
        Picasso.get().load(dp).into(holder.cuserImg);
        holder.commentUser.setText(comments.get(position).getUser_name());
        holder.comment.setText(comments.get(position).getComment_msg());

        // delete the comments
        if(GetPresistenceData.getMyIMEI(mcontext).equals(comments.get(position).getUser_imei())) {
            holder.postMenu.setVisibility(View.VISIBLE);
        }

        // click on post menu image to open edit and delete posts
        holder.postMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(mcontext, holder.postMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(R.id.edit==item.getItemId())
                        {

                        }
                        else if(R.id.delete==item.getItemId())
                        {

                                deleteData(comments.get(position).getSr(),holder,position);
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

    } //onBind closer

    private void deleteData(final String post_id, final Holder holder, final  int position) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue = new RequestQueue(cache1, network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.delete_comment, new Response.Listener<String>() {
            @Override
            public void onResponse(String response6) {
                deleteRes(response6,holder,position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("post_id", post_id);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void deleteRes(String response6,Holder holder, final  int position) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response6);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                comments.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), comments.size());

            }
            else{

                Toast.makeText(mcontext, "Network issue", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return comments.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private CircleImageView cuserImg;
        private TextView commentUser;
        private TextView comment;
        private ImageView postMenu;





        Holder(@NonNull View itemView) {
            super(itemView);
            cuserImg = (CircleImageView) itemView.findViewById(R.id.cuser_img);
            commentUser = (TextView) itemView.findViewById(R.id.comment_user);
            comment = (TextView) itemView.findViewById(R.id.comments);
            postMenu = (ImageView) itemView.findViewById(R.id.post_menu);
        }
    }
}
