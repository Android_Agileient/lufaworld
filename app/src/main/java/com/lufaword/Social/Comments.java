package com.lufaword.Social;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.github.chrisbanes.photoview.PhotoView;
import com.lufaword.Async.saveNotification;
import com.lufaword.Configuration.Config;
import com.lufaword.Login.Register;
import com.lufaword.Login.Terms;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Comments extends AppCompatActivity {
    private CircleImageView userProfilePic;
    private TextView userName;
    private ImageView postMenu;
    private TextView date;
    private TextView postText;
    private PhotoView postImg;
    private LinearLayout commentLay;
    private TextView commentsTxt;
    private LinearLayout commentEditboxLay;
    private EditText enterComment;
    private Button saveCmtBtn;
    private RecyclerView recCmt;
    String Name,Dp,Date,Post_txt,Post_img,Post_id,comment_txt,User_imei;
    RequestQueue queue;
    private ArrayList<Comment2> comments;
    private ImageView GoBack;








    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_lay);
        userProfilePic = (CircleImageView) findViewById(R.id.user_profile_pic);
        userName = (TextView) findViewById(R.id.user_name);
        postMenu = (ImageView) findViewById(R.id.post_menu);
        date = (TextView) findViewById(R.id.date);
        postText = (TextView) findViewById(R.id.post_text);
        postImg = (PhotoView) findViewById(R.id.post_img);
        commentLay = (LinearLayout) findViewById(R.id.commentLay);
        commentsTxt = (TextView) findViewById(R.id.comments_txt);
        commentEditboxLay = (LinearLayout) findViewById(R.id.comment_editbox_lay);
        enterComment = (EditText) findViewById(R.id.enterComment);
        saveCmtBtn = (Button) findViewById(R.id.save_cmt_btn);
        recCmt = (RecyclerView) findViewById(R.id.recCmt);
        GoBack = (ImageView) findViewById(R.id.GoBack);


        //Receive Intents
        Intent intent=getIntent();
        Name=intent.getStringExtra("Name");
        Dp=intent.getStringExtra("Dp");
        Date=intent.getStringExtra("Date");
        Post_txt=intent.getStringExtra("Post_txt");
        Post_img=intent.getStringExtra("Post_img");
        Post_id=intent.getStringExtra("Post_id");
        User_imei=intent.getStringExtra("User_imei");

        // set Constraints
        Picasso.get().load(Dp).into(userProfilePic);
        userName.setText(Name);
        date.setText(Date);
        postText.setText(Post_txt);

        if(!Post_img.equals("NA")) {
            Picasso.get().load(Post_img).into(postImg);
            postImg.setVisibility(View.VISIBLE);
        }

        // initilize array
        comments=new ArrayList<>();

        // Load Comments
        loadComments(Post_id);

        // save comments
        saveCmtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comment_txt=enterComment.getText().toString();
                if (comment_txt.length() < 2) {
                    Toast.makeText(Comments.this, "Required minimum 2 words", Toast.LENGTH_SHORT).show();
                    enterComment.requestFocus();
                } else if (comment_txt.length() > 100) {

                    Toast.makeText(getApplicationContext(), "Required maximum 100 words", Toast.LENGTH_SHORT).show();
                    enterComment.requestFocus();
                }
                else
                {
                    String imei= GetPresistenceData.getMyIMEI(getApplicationContext());

                    String User_name=GetPresistenceData.getName(getApplicationContext());

                    saveComments(imei,Post_id,User_name,comment_txt);


                }
            }
        });

        GoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Comments.this,MainActivity.class);
                startActivity(intent);

            }
        });


    } // onCreate closer

    private void loadComments(String post_id) {
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue = new RequestQueue(cache1, network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.show_comments, new Response.Listener<String>() {
            @Override
            public void onResponse(String response6) {
                parseData6(response6);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("video_id", Post_id);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData6(String response6) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response6);
            String Status=jsonObject.getString("status");
            JSONArray jsonArray=jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                String sr = jsonObject1.getString("sr");
                String user_imei = jsonObject1.getString("imei");
                String video_id = jsonObject1.getString("video_id");
                String user_name = jsonObject1.getString("user_name");
                String comment_msg = jsonObject1.getString("comment_msg");
                String profile_pic = jsonObject1.getString("profile_pic");


                Comment2 comment2 = new Comment2(sr,user_imei,video_id,user_name,comment_msg,profile_pic,Post_id);
                comments.add(comment2);

                CmtRec cmtRec = new CmtRec(getApplicationContext(), comments);
                final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                recCmt.setLayoutManager(layoutManager);
                recCmt.setHasFixedSize(true);
                recCmt.setAdapter(cmtRec);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void saveComments(final String imei, final String post_id, final String user_name, final String comment_txt) {
        Cache cache1 = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.save_Comment, new Response.Listener<String>() {
            @Override
            public void onResponse(String response5) {
                parseData5(response5,post_id);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",imei);
                mydata.put("video_id",post_id);
                mydata.put("user_name",user_name);
                mydata.put("comment_msg",comment_txt);
                mydata.put("profile_pic",GetPresistenceData.getProfilepic(getApplicationContext()));
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData5(String response5, String post_id) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response5);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(getApplicationContext(), "Comment Saved", Toast.LENGTH_SHORT).show();
                enterComment.setText("");
                String imei=GetPresistenceData.getMyIMEI(getApplicationContext());
               comments.clear();
                // Load Comments
                loadComments(Post_id);
               new saveNotification(User_imei,GetPresistenceData.getName(getApplicationContext()),", Comment on your post ","1",getApplicationContext(),"comment").execute();
            }
            else{
                Toast.makeText(getApplicationContext(), "Network Issue | Try again", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


    // backKey pressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((keyCode==KeyEvent.KEYCODE_BACK))
        {
            Intent back1Intent=new Intent(Comments.this, MainActivity.class);
            startActivity(back1Intent);
            overridePendingTransition(R.anim.enter,R.anim.exit);
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }
}
