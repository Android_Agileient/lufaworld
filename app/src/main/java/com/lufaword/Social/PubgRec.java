package com.lufaword.Social;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

class PubgRec extends RecyclerView.Adapter<PubgRec.Holder> {
    Context mcontext;
    ArrayList<PUBG>pubg;
    PubgRec(Context mcontext, ArrayList<PUBG> pubg) {
        this.mcontext=mcontext;
        this.pubg=pubg;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.public_group,parent,false);
        return new PubgRec.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        holder.groupName.setText(pubg.get(position).getGroup_name());
        Picasso.get().load(pubg.get(position).getUser_pic()).into(holder.userProfilePic);
        holder.userName.setText("Group created by "+pubg.get(position).getName());
        holder.grpDesc.setText(pubg.get(position).getGroup_desc());
        // detegroup button will work as Join Group button

        holder.join_btn.setVisibility(View.VISIBLE);
        // click on delete groupbtn for join the group
        holder.join_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new joinGroup(holder, GetPresistenceData.getMyIMEI(mcontext),pubg.get(position).getSr(),pubg.get(position).getUser_pic(),GetPresistenceData.getName(mcontext),pubg.get(position).getGrp_checksum()).execute();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pubg.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private TextView groupName;
        private CircleImageView userProfilePic;
        private TextView userName;
        private TextView grpDesc;
        private ImageView joinBtn;
        private TextView join_btn;
        private ImageView chatGroupBtn;



        Holder(@NonNull View itemView) {
            super(itemView);
            groupName = (TextView) itemView.findViewById(R.id.group_name);
            userProfilePic = (CircleImageView) itemView.findViewById(R.id.user_profile_pic);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            grpDesc = (TextView) itemView.findViewById(R.id.grp_desc);
            joinBtn = (ImageView) itemView.findViewById(R.id.delete_group_btn);
            join_btn = (TextView) itemView.findViewById(R.id.join_btn);
            chatGroupBtn = (ImageView) itemView.findViewById(R.id.chat_group_btn);
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class joinGroup extends AsyncTask<String,String,String> {
        Holder holder;
        String myIMEI,sr,profilepic,name,grp_checksum;
        joinGroup(Holder holder, String myIMEI, String sr, String profilepic, String name, String grp_checksum) {
            this.myIMEI=myIMEI;
            this.holder=holder;
            this.sr=sr;
            this.profilepic=profilepic;
            this.name=name;
            this.grp_checksum=grp_checksum;
        }

        @Override
        protected String doInBackground(String... strings) {
            RequestQueue queue1;
            Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue1=new RequestQueue(cache1,network1);
            queue1.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.join_group, new Response.Listener<String>() {
                @Override
                public void onResponse(String response3) {
                    parseData1(response3,holder);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    // no need to pass imei but as security i passed it
                    mydata.put("user_imei",myIMEI);
                    mydata.put("group_id",sr);
                    mydata.put("user_pic",profilepic);
                    mydata.put("user_name",name);
                    mydata.put("grp_checksum",grp_checksum);
                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue1.add(request);
            return null;
        }
    }

    private void parseData1(String response3,Holder holder) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response3);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "You joined new group", Toast.LENGTH_SHORT).show();
                pubg.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), pubg.size());
            }
            else
            {
                Toast.makeText(mcontext, "Join group failed", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
