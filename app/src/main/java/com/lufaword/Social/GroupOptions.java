package com.lufaword.Social;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.lufaword.Friends.MyFriends;
import com.lufaword.MainActivity;
import com.lufaword.R;

public class GroupOptions extends AppCompatActivity implements OwnGroup.OnFragmentInteractionListener,PublicGroup.OnFragmentInteractionListener {
    private LinearLayout mainLay2;
    private TabLayout tabLay1;
    private ViewPager viewPager3;
    private ImageView backArrow;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_group_options);
        mainLay2 = (LinearLayout) findViewById(R.id.mainLay2);
        tabLay1 = (TabLayout) findViewById(R.id.tabLay1);
        viewPager3 = (ViewPager) findViewById(R.id.viewPager3);
        backArrow = (ImageView) findViewById(R.id.backArrow);

        // first make Adapeter class and then set
        PagerAdapter1 pagerAdapter1=new PagerAdapter1(getSupportFragmentManager(),tabLay1.getTabCount());
        viewPager3.setAdapter(pagerAdapter1);
        // add page change listner on viewpager for slide
        viewPager3.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLay1));
        tabLay1.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager3.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

// click on back arrow to go back
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back1Intent=new Intent(GroupOptions.this, MainActivity.class);
                startActivity(back1Intent);
                finish();
            }
        });

    } //onCreate closer

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
