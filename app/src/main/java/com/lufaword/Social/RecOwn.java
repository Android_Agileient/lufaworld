package com.lufaword.Social;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.Friends.FrndRec2;
import com.lufaword.Friends.Frnds;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

class RecOwn extends RecyclerView.Adapter<RecOwn.Holder> {
    Context mcontext;
    ArrayList<Own>own;
    public RecOwn(Context mcontext, ArrayList<Own> own) {
        this.mcontext=mcontext;
        this.own=own;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.own_groups,parent,false);
        return new RecOwn.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        holder.groupName.setText(own.get(position).getGroup_name());
        Picasso.get().load(own.get(position).getUser_pic()).into(holder.userProfilePic);
        holder.userName.setText("Created by "+own.get(position).getUser_name());
        holder.grpDesc.setText(own.get(position).getGroup_desc());
        // show delete group button if group owner id or imei is the same
        if(GetPresistenceData.getMyIMEI(mcontext).equals(own.get(position).getOwner()))
        {
            holder.deleteGroupBtn.setVisibility(View.VISIBLE);
            holder.chatGroupBtn.setVisibility(View.VISIBLE);
        }
        else
        {
           holder.leaveGroupBtn.setVisibility(View.VISIBLE);
           holder.chatGroupBtn.setVisibility(View.VISIBLE);
        }
        // delete group
        holder.deleteGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteGroup(holder,GetPresistenceData.getMyIMEI(mcontext),own.get(position).getSr(),own.get(position).getGrp_checksum());
            }
        });
        // leave the group
        holder.leaveGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leaveGroup(GetPresistenceData.getMyIMEI(mcontext),holder,own.get(position).getSr(),GetPresistenceData.getName(mcontext),own.get(position).getGrp_checksum());
            }
        });

    }

    private void leaveGroup(final String myIMEI, final Holder holder, final String sr, final String name, final String grp_checksum) {
        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.leave_group, new Response.Listener<String>() {
            @Override
            public void onResponse(String response2) {
                leaveRes(response2,holder);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("user_imei",myIMEI);
                mydata.put("group_id",sr);
                mydata.put("user_name",name);
                mydata.put("grp_checksum",grp_checksum);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);

    }

    private void leaveRes(String response2, Holder holder) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response2);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "Group Leaved", Toast.LENGTH_SHORT).show();
                own.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), own.size());
            }
            else
            {
                Toast.makeText(mcontext, "Unable to leave group", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void deleteGroup(final Holder holder, final String myIMEI, final String sr, final String grp_checksum) {
        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.delete_group, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                deleteRes(response1,holder);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("user_imei",myIMEI);
                mydata.put("group_id",sr);
                mydata.put("grp_checksum",grp_checksum);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void deleteRes(String response1, Holder holder) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "Group deleted", Toast.LENGTH_SHORT).show();
                own.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), own.size());
            }
            else
            {
                Toast.makeText(mcontext, "Unable to remove group", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


    @Override
    public int getItemCount() {
        return own.size();
    }



    class Holder extends RecyclerView.ViewHolder {
        private TextView groupName;
        private CircleImageView userProfilePic;
        private TextView userName;
        private TextView grpDesc;
        private ImageView deleteGroupBtn;
        private ImageView leaveGroupBtn;
        private ImageView chatGroupBtn;



        Holder(@NonNull View itemView) {
            super(itemView);
            groupName = (TextView) itemView.findViewById(R.id.group_name);
            userProfilePic = (CircleImageView) itemView.findViewById(R.id.user_profile_pic);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            grpDesc = (TextView) itemView.findViewById(R.id.grp_desc);
            deleteGroupBtn = (ImageView) itemView.findViewById(R.id.delete_group_btn);
            leaveGroupBtn = (ImageView) itemView.findViewById(R.id.leave_group_btn);
            chatGroupBtn = (ImageView) itemView.findViewById(R.id.chat_group_btn);
        }
    }
}
