package com.lufaword.Social;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;

import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OwnGroup.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OwnGroup#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OwnGroup extends Fragment {
    private TextView heading;
    private RecyclerView recOwn;
    private EditText groupEditbox;
    private Button createBtn;
    private Button groupSave;
    private LinearLayout alerts;
ArrayList<Own>own;
    private SwipeRefreshLayout swipe;
    private LinearLayout editLay;





    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public OwnGroup() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OwnGroup.
     */
    // TODO: Rename and change types and number of parameters
    public static OwnGroup newInstance(String param1, String param2) {
        OwnGroup fragment = new OwnGroup();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
own=new ArrayList<>();
own.clear();






        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_own_group,container,false);
        heading = (TextView) view.findViewById(R.id.heading);
        recOwn = (RecyclerView) view.findViewById(R.id.rec_own);
        groupEditbox = (EditText) view.findViewById(R.id.group_editbox);
        createBtn = (Button) view.findViewById(R.id.create_btn);
        groupSave = (Button) view.findViewById(R.id.group_save);
        alerts = (LinearLayout) view.findViewById(R.id.alerts);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        editLay = (LinearLayout) view.findViewById(R.id.edit_lay);

        // swipe on click refresh
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                own.clear();
                loadMyGroups(GetPresistenceData.getMyIMEI(getContext()));
                swipe.setRefreshing(false);
            }
        });

        // click on create group
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createBtn.setVisibility(View.GONE);
                editLay.setVisibility(View.VISIBLE);


            }
        });
        // save group name
        groupSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String group_name=groupEditbox.getText().toString();
                if(group_name.length()<=2)
                {
                    Toast.makeText(getContext(), "Require minimum 3 word as group name", Toast.LENGTH_SHORT).show();
                    groupEditbox.requestFocus();
                }
                else if(group_name.contains("fuck") || group_name.contains("FUCK") || group_name.contains("pussy") || group_name.contains("PUSSY") || group_name.contains("SEXY") || group_name.contains("sexy")) 
                {
                    Toast.makeText(getContext(), "Can't use abusing words", Toast.LENGTH_SHORT).show();
                    groupEditbox.requestFocus();
                }
                else
                {
                    saveGroup(group_name, GetPresistenceData.getMyIMEI(getContext()),GetPresistenceData.getName(getContext()),"Lufa word groups",GetPresistenceData.getProfilepic(getContext()));
                }
            }
        });


        // load my groups
        loadMyGroups(GetPresistenceData.getMyIMEI(getContext()));
        return view;
    }





    private void loadMyGroups(final String myIMEI) {
        RequestQueue queue2;
        Cache cache1 = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue2=new RequestQueue(cache1,network1);
        queue2.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.my_groups, new Response.Listener<String>() {
            @Override
            public void onResponse(String response4) {
                parseData4(response4);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("imei",myIMEI);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);

    }

    private void parseData4(String response4) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response4);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String sr = jsonObject1.getString("sr");
                    String users_count = jsonObject1.getString("users_count");
                    String group_name = jsonObject1.getString("group_name");
                    String group_desc = jsonObject1.getString("group_desc");
                    String user_imei = jsonObject1.getString("user_imei");
                    String user_name = jsonObject1.getString("user_name");
                    String date = jsonObject1.getString("date");
                    String owner = jsonObject1.getString("owner");
                    String grp_checksum = jsonObject1.getString("grp_checksum");
                    String user_pic = jsonObject1.getString("user_pic");


                    Own own2 = new Own(sr,users_count,group_name,group_desc,user_imei,user_name,date,owner,grp_checksum,user_pic);
                    own.add(own2);
                    recOwn.setVisibility(View.VISIBLE);
                    alerts.setVisibility(View.GONE);

                    RecOwn recOwn1 = new RecOwn(getContext(), own);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    recOwn.setLayoutManager(layoutManager);
                    recOwn.setHasFixedSize(true);
                    recOwn.setAdapter(recOwn1);
                    //suggestFRec.scrollToPosition(5); //use to focus the item with index
                    //rSuugested.notifyDataSetChanged();

                }
            }
            else
            {
                Toast.makeText(getContext(), "No group Found", Toast.LENGTH_SHORT).show();
                recOwn.setVisibility(View.GONE);
                alerts.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void saveGroup(final String groupName, final String imei, final String user_name, final String desc, final String user_pic) {
        RequestQueue queue1;
        Cache cache1 = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue1=new RequestQueue(cache1,network1);
        queue1.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.create_group, new Response.Listener<String>() {
            @Override
            public void onResponse(String response3) {
                parseData3(response3);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                // no need to pass imei but as security i passed it
                mydata.put("user_imei",imei);
                mydata.put("user_name",user_name);
                mydata.put("group_name",groupName);
                mydata.put("group_desc",desc);
                mydata.put("user_pic",user_pic);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData3(String response3) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response3);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(getContext(), "Group Created", Toast.LENGTH_SHORT).show();
                editLay.setVisibility(View.GONE);
                createBtn.setVisibility(View.VISIBLE);
                own.clear();
                loadMyGroups(GetPresistenceData.getMyIMEI(getContext()));
                // call group update method
            }
            else if(Status.equals("2"))
            {
                Toast.makeText(getContext(), "Group Already Created", Toast.LENGTH_SHORT).show();
                groupEditbox.setText("");
                groupEditbox.requestFocus();
            }
            else
            {
                Toast.makeText(getContext(), "Group Creation Failed", Toast.LENGTH_SHORT).show();
                editLay.setVisibility(View.GONE);
                createBtn.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
