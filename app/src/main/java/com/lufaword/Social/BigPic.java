package com.lufaword.Social;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.lufaword.Configuration.Config;
import com.lufaword.Fragments.Tab1;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class BigPic  extends AppCompatActivity implements Tab1.OnFragmentInteractionListener {
    private LinearLayout postLay;
    private TextView closePostLay;
    private EditText shareIdeaText;
    private TextView postBtn;
    private ImageView imgView;
    String image,imei,text,post_id;
    private TextView cameraBtn;
    ProgressDialog pd;
    String cameravalue="";
    int GALLERY = 2;
    RequestQueue queue;
    private static final int REQUEST_STORAGE_PERMISSION = 100;
    private ImageView clearImage;
    private LinearLayout postLay3;
    private TextView photos;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_post);
        postLay = (LinearLayout) findViewById(R.id.post_lay);
        closePostLay = (TextView) findViewById(R.id.close_post_lay);
        shareIdeaText = (EditText) findViewById(R.id.share_idea_text);
        postBtn = (TextView) findViewById(R.id.post_btn);
        imgView = (ImageView) findViewById(R.id.img_view);
        cameraBtn = (TextView) findViewById(R.id.camera_btn);
        clearImage = (ImageView) findViewById(R.id.clear_image);
        postLay3 = (LinearLayout) findViewById(R.id.post_lay3);
        photos = (TextView) findViewById(R.id.photos);


        if(shareIdeaText.requestFocus()==true)
        {
            postBtn.setEnabled(true);
        }
        // initilize progress dialogue box
        pd=new ProgressDialog(this);
        pd.setTitle("Updating post");
        pd.setMessage("Please wait...");

        // receive intents
        Intent intent=getIntent();
        image=intent.getStringExtra("image");
        imei=intent.getStringExtra("imei");
        text=intent.getStringExtra("text");
        post_id=intent.getStringExtra("post_id");

        // set Constraints on layout
        shareIdeaText.setText(text);
        shareIdeaText.requestFocus();
        if(!image.equals("NA"))
        {
            Picasso.get().load(image).into(imgView);
            imgView.setVisibility(View.VISIBLE);
            clearImage.setVisibility(View.VISIBLE);
        }

        // clear loaded image
        clearImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image="NA";
                clearImage.setVisibility(View.GONE);
                imgView.setVisibility(View.GONE);


            }
        });

        // click on camera to capture image
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureimagefromcamera();


            }
        });
        // click on photos to pic image from Gallery
        photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gallery();


            }
        });
        // closePostLay click listner to go back on previous page
        closePostLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        // ssave the post on server
        // send the post to server
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate status data
                String ideas_text=shareIdeaText.getText().toString();
                if(ideas_text.length()<3)
                {
                    Toast.makeText(BigPic.this, "Your ideas are too small worded", Toast.LENGTH_SHORT).show();
                    shareIdeaText.requestFocus();
                }
                else if(ideas_text.length()>250)
                {
                    Toast.makeText(BigPic.this, "Text limit maximum exceed than 250 words", Toast.LENGTH_SHORT).show();
                    shareIdeaText.requestFocus();
                }
                else
                {
                    if(image.equals(""))
                    {
                        image="NA";
                        String imei= GetPresistenceData.getMyIMEI(getApplicationContext());
                        String email=GetPresistenceData.getEmail(getApplicationContext());
                        String name=GetPresistenceData.getName(getApplicationContext());
                        uploadtoserverr(image,ideas_text,imei,email,name);
                    }
                    else
                    {
                        pd.setTitle("Updating your post");
                        pd.setMessage("Please wait....");
                        pd.setCancelable(true);
                        pd.show();
                        String imei=GetPresistenceData.getMyIMEI(getApplicationContext());
                        String email=GetPresistenceData.getEmail(getApplicationContext());
                        String name=GetPresistenceData.getName(getApplicationContext());

                        uploadtoserverr(image,ideas_text,imei,email,name);
                    }

                }
            }
        });




    } //onCreate closer

    private void gallery() {
        if(PackageManager.PERMISSION_GRANTED !=
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if(ActivityCompat.shouldShowRequestPermissionRationale((Activity) getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }else {
//Yeah! I want both block to do the same thing, you can write your own logic, but this works for me.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }
        }else {
//Permission Granted, lets go pick photo
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // this flag is important in Mi mobiles
            // intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, GALLERY);
            pd.show();
            pd.setMessage("Uploading...");
        }
    }

    private void captureimagefromcamera() {
        cameravalue="2";
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    private void uploadtoserverr(final String image, final String ideas_text, final String imei, final String email, final String name) {
        Cache cache1 = new DiskBasedCache(getApplicationContext().getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.update_post, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                pd.dismiss();
                parseData1(response1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",imei);
                mydata.put("email",email);
                mydata.put("name",name);
                mydata.put("post_txt",ideas_text);
                mydata.put("post_img",image);
                mydata.put("shared","0");
                mydata.put("shared_name","0");
                mydata.put("post_id",post_id);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData1(String response1) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                pd.dismiss();

                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                imgView.setImageDrawable(null);
                postLay.setVisibility(View.GONE);

                // send back to the main activity
               /* FragmentManager manager = this.getSupportFragmentManager();
                Tab1 tab1 = new Tab1();
                manager.beginTransaction().replace(R.id.viewPager2,tab1)
                        .commit();*/
                Tab1.loadPost(getApplicationContext(),"T","T");
               finish();

                // load posts
                //posts.clear();
                //posts.remove(posts.size()-1);
                //loadPost(getApplicationContext(),"T","T");
            }
            else{
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA && data != null) {
//imgage captured
            if(cameravalue.equals("2")) {
                Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                assert bitmap != null;
                imageToString(bitmap);
                imgView.setImageBitmap(bitmap);
                imgView.setVisibility(View.VISIBLE);
                //capture_image.setImageBitmap(bitmap);
            }

        }

        else if (requestCode == GALLERY && data != null && data.getData() != null ) {
            if (resultCode == RESULT_OK) {

                Uri contentURI = data.getData();
                Log.e("Internal Uri", String.valueOf(contentURI));
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);
                    Log.e("bitmap....", String.valueOf(bitmap));
                    imageToString2(bitmap);
                    imgView.setImageBitmap(bitmap);
                    imgView.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                //sendData();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Failed to select the image.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to select image",
                        Toast.LENGTH_LONG).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void imageToString2(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        Log.e("base64 Conversion",image);
        pd.dismiss();
    }

    private void imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        image = Base64.encodeToString(imgbytes, Base64.DEFAULT);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
