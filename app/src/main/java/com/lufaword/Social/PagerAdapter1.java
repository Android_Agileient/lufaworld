package com.lufaword.Social;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


class PagerAdapter1 extends FragmentStatePagerAdapter {
    private int tabcount;
    PagerAdapter1(FragmentManager fms, int tabCount) {
        super(fms);
        this.tabcount=tabCount;
    }

    @Override
    public int getCount() {
        return tabcount;
    }




    @NonNull
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new OwnGroup();

            case 1:
                return new PublicGroup();
            default:
                return null;
        }
    }
}
