package com.lufaword.Social;

class Own {
    String sr,users_count,group_name,group_desc,user_imei,user_name,date,owner,grp_checksum,user_pic;
    Own(String sr, String users_count, String group_name, String group_desc, String user_imei, String user_name, String date, String owner, String grp_checksum,String user_pic) {
        this.sr=sr;
        this.users_count=users_count;
        this.group_name=group_name;
        this.group_desc=group_desc;
        this.user_imei=user_imei;
        this.user_name=user_name;
        this.date=date;
        this.owner=owner;
        this.grp_checksum=grp_checksum;
        this.user_pic=user_pic;
    }
    // getter and setter method

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getUsers_count() {
        return users_count;
    }

    public void setUsers_count(String users_count) {
        this.users_count = users_count;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_desc() {
        return group_desc;
    }

    public void setGroup_desc(String group_desc) {
        this.group_desc = group_desc;
    }

    public String getUser_imei() {
        return user_imei;
    }

    public void setUser_imei(String user_imei) {
        this.user_imei = user_imei;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getGrp_checksum() {
        return grp_checksum;
    }

    public void setGrp_checksum(String grp_checksum) {
        this.grp_checksum = grp_checksum;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }
}
