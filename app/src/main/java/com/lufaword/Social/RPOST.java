package com.lufaword.Social;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.github.chrisbanes.photoview.PhotoView;
import com.lufaword.BuildConfig;
import com.lufaword.Configuration.Config;
import com.lufaword.Profile.UserProfile;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class RPOST extends RecyclerView.Adapter<RPOST.Holder> {
    private ArrayList<Posts> posts;
    private Context mcontext;
    private RequestQueue queue;
    private ProgressDialog pd;
    int likes;
    public RPOST(Context mcontext, ArrayList<Posts> posts) {
        this.mcontext=mcontext;
        this.posts=posts;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view= LayoutInflater.from(mcontext).inflate(R.layout.my_posts,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        // initilize pd
        pd=new ProgressDialog(mcontext);
        pd.setTitle("Processing...");
        pd.setMessage("Please wait");
        likes=Integer.parseInt(posts.get(position).getTotal_like());
        holder.likeCount.setText(posts.get(position).getTotal_like());
        // post menu show only on my post not at all
        if(posts.get(position).getImei().equals(GetPresistenceData.getMyIMEI(mcontext)))
        {
            holder.postMenu.setVisibility(View.VISIBLE);
        }

        if(posts.get(position).getPost_img().equals("NA"))
        {
            holder.postImg.setVisibility(View.GONE);
        }
        else {

            Picasso.get().load(posts.get(position).getPost_img()).into(holder.postImg);
        }

        if(posts.get(position).getShared().equals("0"))
        {
            holder.userName.setText(posts.get(position).getName());
        }
        else
        {
            holder.userName.setText(posts.get(position).getName()+" Shared post of "+posts.get(position).getShared_name());
        }

        // if total likes count is more than zero then make visible views and put total counts
      /*  if(posts.get(position).getPost_like().equals("0")) {
            holder.like.setVisibility(View.VISIBLE);

        }
        else
        {
            holder.unlike.setVisibility(View.VISIBLE);
           // holder.likeCount.setBackgroundResource(R.drawable.white_border);
        }*/
        holder.like.setVisibility(View.VISIBLE);

        // if total comments count is more than zero then make visible no of comments and put total comments counts
        if(posts.get(position).getPost_comments().equals("0")) {

        }
        else
        {
            holder.cmtCount.setText(posts.get(position).getPost_comments());
           // holder.cmtCount.setBackgroundResource(R.drawable.green_double_rounded);
        }
        // set other parameters
        final String user_img = posts.get(position).getDp();
        Picasso.get().load(user_img).into(holder.userProfilePic);
        holder.postText.setText(posts.get(position).getPost_txt());
        holder.date.setText("Posted on "+posts.get(position).getDate1());

        // set red color on like button
        if(posts.get(position).getLike().equals("1"))
        {

        }

  /*      // click on image and show it on a big
        holder.postImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mcontext, UserProfile.class);
                intent.putExtra("image",posts.get(position).getPost_img());
                intent.putExtra("type","1");
                intent.putExtra("imei",posts.get(position).getImei());
                intent.putExtra("username",posts.get(position).getName());
                intent.putExtra("email",posts.get(position).getEmail());

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(intent);
            }
        });*/
            // click on user profile pic
        holder.userProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, UserProfile.class);
                intent.putExtra("image",user_img);
                intent.putExtra("type","1");
                intent.putExtra("imei",posts.get(position).getImei());
                intent.putExtra("username",posts.get(position).getName());
                intent.putExtra("email",posts.get(position).getEmail());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(intent);
            }
        });

        // click on unlike button
        holder.unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unlikeholder(GetPresistenceData.getMyIMEI(mcontext), posts.get(position).getSr(), holder,position);
            }
        });
        // click on shared
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        // click on post menu image to open edit and delete posts
        holder.postMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(mcontext, holder.postMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(R.id.edit==item.getItemId())
                        {
                            if(GetPresistenceData.getMyIMEI(mcontext).equals(posts.get(position).getImei())) {
                                Intent intent = new Intent(mcontext, BigPic.class);
                                intent.putExtra("image", posts.get(position).getPost_img());
                                intent.putExtra("imei", posts.get(position).getImei());
                                intent.putExtra("post_id", posts.get(position).getSr());
                                intent.putExtra("text", posts.get(position).getPost_txt());
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mcontext.startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(mcontext, "Sorry, this is not your post", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if(R.id.delete==item.getItemId())
                        {
                            if(GetPresistenceData.getMyIMEI(mcontext).equals(posts.get(position).getImei())) {

                                deletePost(posts.get(position).getSr(), holder);
                            }
                            else
                            {
                                Toast.makeText(mcontext,"You cant delete | Its not your post", Toast.LENGTH_SHORT).show();
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

        // click on like button
        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(posts.get(position).getLike().equals("1"))
                {
                    Toast.makeText(mcontext, "You already liked", Toast.LENGTH_SHORT).show();
                }
                else {
                    likePost(GetPresistenceData.getMyIMEI(mcontext), posts.get(position).getSr(), holder,position);
                }
            }
        });
        // click on comment for another layout
        holder.commentBoxLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext,Comments.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Name",posts.get(position).getName());
                intent.putExtra("Dp",posts.get(position).getDp());
                intent.putExtra("Date",posts.get(position).getDate1());
                intent.putExtra("Post_txt",posts.get(position).getPost_txt());
                intent.putExtra("Post_img",posts.get(position).getPost_img());
                intent.putExtra("Post_id",posts.get(position).getSr());
                intent.putExtra("User_imei",posts.get(position).getImei());
                mcontext.startActivity(intent);
            }
        });
        // click on share button
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mcontext);
                pictureDialog.setTitle("Select Action");
                String[] pictureDialogItems = {
                        "Share on Whatsapp",
                        "Share on Timeline"};
                pictureDialog.setItems(pictureDialogItems,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                switch (i) {
                                    case 0:
                                        if(posts.get(position).getPost_img().equals("NA"))
                                        {
                                            Intent intent=new Intent(Intent.ACTION_SEND);
                                            intent.setType("text/*");
                                            //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                                            String data = posts.get(position).getName()+" sharing recent post \n\n "+posts.get(position).getPost_txt()+" \n \n***********\n "+"Likes: "+posts.get(position).getTotal_like()+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName());
                                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            intent.putExtra(Intent.EXTRA_TEXT,data);
                                            intent.setPackage(Config.whatsapp_package);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            // for particular choose we will set getPackage()
                                            /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
                                            mcontext.startActivity(intent);
                                        }
                                        else
                                        {
                                            PackageManager manager = mcontext.getPackageManager();
                                            Picasso.get().load(posts.get(position).getPost_img()).into(new Target() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                    /* uri1=Uri.parse(Paths+File.separator+"10.jpg");*/
                                                    Intent intent=new Intent(Intent.ACTION_SEND);
                                                    intent.setType("image/*");
                                                    //intent.putExtra(intent.EXTRA_SUBJECT,"Insert Something new");
                                                    String data = posts.get(position).getName()+" sharing recent post \n\n "+posts.get(position).getPost_txt()+" \n \n***********\n "+"Likes: "+posts.get(position).getTotal_like()+"\n***********\n\n For more information install our application and join Lufa World: \n"+ Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName());
                                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                                    intent.putExtra(Intent.EXTRA_TEXT,data);
                                                    intent.putExtra(Intent.EXTRA_STREAM,getLocalBitmapUri(bitmap,mcontext));
                                                    intent.setPackage(Config.whatsapp_package);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    // for particular choose we will set getPackage()
                                                    /*startActivity(intent.createChooser(intent,"Share Via"));*/// this code use for universal sharing
                                                    mcontext.startActivity(intent);
                                                }

                                                @Override
                                                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                                    Toast.makeText(mcontext, "Load Image Failed", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                                }
                                            });

                                            // end Share code
                                        }
                                        pd.setMessage("Uploading...");
                                        break;
                                    case 1:
                                        timeline(position);

                                        pd.setMessage("Uploading...");
                                        break;
                                }
                            }
                        });
                pictureDialog.show();
            }
        });


    }  // onbind closer

    private void timeline(final int position) {
        if (GetPresistenceData.getMyIMEI(mcontext).equals(posts.get(position).getImei()))
        {
            Toast.makeText(mcontext, "You cant shared your post || Share other's post", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(mcontext, "Sharing...", Toast.LENGTH_SHORT).show();
            Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue = new RequestQueue(cache1, network1);
            queue.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.sharePost, new Response.Listener<String>() {
                @Override
                public void onResponse(String response1) {
                    parseData1(response1);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mcontext,"Server not responding", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    mydata.put("imei", GetPresistenceData.getMyIMEI(mcontext));
                    mydata.put("email", GetPresistenceData.getEmail(mcontext));
                    mydata.put("name", GetPresistenceData.getName(mcontext));
                    mydata.put("post_txt", posts.get(position).getPost_txt());
                    mydata.put("post_img", posts.get(position).getPost_img());
                    mydata.put("shared", "1");
                    mydata.put("shared_name", posts.get(position).getName());
                    mydata.put("User_imei", posts.get(position).getImei());
                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue.add(request);
        }
    }


    private Uri getLocalBitmapUri(Bitmap bmp, Context context) {

        Uri uriimg = null;

        try {

            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LufaWorld" + System.currentTimeMillis() + ".png");

            FileOutputStream out = new FileOutputStream(file);

            bmp.compress(Bitmap.CompressFormat.PNG, 50, out);

            out.close();

            /*uriimg = Uri.fromFile(file);*/
            uriimg = FileProvider.getUriForFile((mcontext),
                    BuildConfig.APPLICATION_ID + ".provider", file);

            refreshGallery(file);
        } catch (IOException e) {

            e.printStackTrace();

        }

        return uriimg;

    }

    private void refreshGallery(File file) {
        Intent intent1 = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent1.setData(Uri.fromFile(file));
        Objects.requireNonNull(mcontext).sendBroadcast(intent1);
    }

    private void unlikeholder(final String myIMEI, final String sr, final Holder holder, final int position) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.unlike_post, new Response.Listener<String>() {
            @Override
            public void onResponse(String response7) {
                parseData7(response7,holder,position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",myIMEI);
                mydata.put("sr",sr);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData7(String response7, Holder holder, int position) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response7);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "Unliked", Toast.LENGTH_SHORT).show();
                likes=likes-1;
                if(likes<0)
                {
                    likes=0;
                    holder.likeCount.setText(String.valueOf(likes));
                }
                else
                {
                    holder.likeCount.setText(String.valueOf(likes));
                }

                holder.unlike.setVisibility(View.GONE);
                holder.like.setVisibility(View.VISIBLE);
                // 1 for save 2 for open 3 for delete
                saveNotification(posts.get(position).getImei(),GetPresistenceData.getName(mcontext),", Unlike your post","1");

            }
            else{
                pd.dismiss();
                Toast.makeText(mcontext, "Network Issue | Try again", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void likePost(final String myIMEI, final String sr, final Holder holder, final int position) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.save_like, new Response.Listener<String>() {
            @Override
            public void onResponse(String response4) {
                parseData4(response4,holder,position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",myIMEI);
                mydata.put("sr",sr);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData4(String response4, Holder holder, int position) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response4);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                Toast.makeText(mcontext, "You Liked it", Toast.LENGTH_SHORT).show();
                likes=likes+1;
                if(likes<0)
                {
                    likes=0;
                    holder.likeCount.setText(String.valueOf(likes));
                }
                else
                {
                    holder.likeCount.setText(String.valueOf(likes));
                }

                //holder.like.setVisibility(View.GONE);
                //holder.unlike.setVisibility(View.VISIBLE);

                // 1 for save 2 for open 3 for delete
                saveNotification(posts.get(position).getImei(),GetPresistenceData.getName(mcontext),", Liked your post","1");
            }
            else if(Status.equals("2")) {

                Toast.makeText(mcontext, "Already Liked", Toast.LENGTH_SHORT).show();
            }
            else{
                pd.dismiss();
                Toast.makeText(mcontext, "Network Issue | Try again", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void saveNotification(final String imei, final String metaName, final String msg, final String type) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.notification, new Response.Listener<String>() {
            @Override
            public void onResponse(String response8) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("imei",imei);
                mydata.put("name",metaName);
                mydata.put("msgg",msg);
                mydata.put("type",type);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void deletePost(final String sr, final Holder holder) {
        Cache cache1 = new DiskBasedCache(mcontext.getCacheDir(), 1024 * 1024);
        Network network1 = new BasicNetwork(new HurlStack());

        queue=new RequestQueue(cache1,network1);
        queue.start();
        StringRequest request = new StringRequest(Request.Method.POST, Config.delete_post, new Response.Listener<String>() {
            @Override
            public void onResponse(String response3) {
                parseData3(response3,holder);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, "Server not responding", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("sr",sr);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData3(String response3, Holder holder) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response3);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                posts.remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), posts.size());
                holder.layout.setVisibility(View.GONE);
            }
            else{
                Toast.makeText(mcontext, "Network Issue", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }

    private void parseData1(String response1) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(response1);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                pd.dismiss();
                Toast.makeText(mcontext, "You past shared on the timeline", Toast.LENGTH_SHORT).show();
            }
            else{
                pd.dismiss();
                Toast.makeText(mcontext, "Update Failed", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        };
    }


    @Override
    public int getItemCount() {
        return posts.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private LinearLayout layout;
        private CircleImageView userProfilePic;
        private TextView userName;
        private ImageView postMenu;
        private TextView date;
        private TextView postText;
        private PhotoView postImg;
        private TextView likeCount;
        private LinearLayout commentBoxLay;
        private TextView cmtCount;
        private TextView shareCount;
        private LinearLayout commentLay;
        private TextView commentsTxt;
        private LinearLayout commentEditboxLay;
        private EditText enterComment;
        private Button saveCmtBtn;
        private RecyclerView recCmt;
        private ImageView like;
        private ImageView comment;
        private ImageView share;
        private ImageView unlike;


        Holder(@NonNull View itemView) {
            super(itemView);
            userProfilePic = (CircleImageView) itemView.findViewById(R.id.user_profile_pic);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            date = (TextView) itemView.findViewById(R.id.date);
            postText = (TextView) itemView.findViewById(R.id.post_text);
            postImg = (PhotoView) itemView.findViewById(R.id.post_img);
            postMenu = (ImageView) itemView.findViewById(R.id.post_menu);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            commentLay = (LinearLayout) itemView.findViewById(R.id.commentLay);
            recCmt = (RecyclerView) itemView.findViewById(R.id.recCmt);
            commentEditboxLay = (LinearLayout) itemView.findViewById(R.id.comment_editbox_lay);
            enterComment = (EditText) itemView.findViewById(R.id.enterComment);
            saveCmtBtn = (Button) itemView.findViewById(R.id.save_cmt_btn);
            commentsTxt = (TextView) itemView.findViewById(R.id.comments_txt);
            likeCount = (TextView) itemView.findViewById(R.id.like_count);
            cmtCount = (TextView) itemView.findViewById(R.id.cmt_count);
            shareCount = (TextView) itemView.findViewById(R.id.share_count);
            commentBoxLay = (LinearLayout) itemView.findViewById(R.id.comment_box_lay);
            recCmt = (RecyclerView) itemView.findViewById(R.id.recCmt);
            like = (ImageView) itemView.findViewById(R.id.like);
            comment = (ImageView) itemView.findViewById(R.id.comment);
            share = (ImageView) itemView.findViewById(R.id.share);
            unlike = (ImageView) itemView.findViewById(R.id.unlike);
        }
    }
}
