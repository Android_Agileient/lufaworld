package com.lufaword.Social;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.lufaword.Configuration.Config;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PublicGroup.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PublicGroup#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PublicGroup extends Fragment {
    private RecyclerView recPublic;
ArrayList<PUBG>pubg;
    private LinearLayout alerts;
    private SwipeRefreshLayout swipe;







    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PublicGroup() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PublicGroup.
     */
    // TODO: Rename and change types and number of parameters
    public static PublicGroup newInstance(String param1, String param2) {
        PublicGroup fragment = new PublicGroup();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pubg=new ArrayList<>();
        pubg.clear();




        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_public_group,container,false);
        recPublic = (RecyclerView) view.findViewById(R.id.rec_public);
        alerts = (LinearLayout) view.findViewById(R.id.alerts);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);

        // refresh on swipe
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // load public groups
                pubg.clear();
                new loadPublicGroups(GetPresistenceData.getMyIMEI(getContext())).execute();
                swipe.setRefreshing(false);
            }
        });
        // load public groups
        new loadPublicGroups(GetPresistenceData.getMyIMEI(getContext())).execute();

        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @SuppressLint("StaticFieldLeak")
    private class loadPublicGroups extends AsyncTask<String,String,String> {
        String myIMEI;
        private loadPublicGroups(String myIMEI) {
            this.myIMEI=myIMEI;

        }

        @Override
        protected String doInBackground(String... strings) {
            RequestQueue queue2;
            Cache cache1 = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
            Network network1 = new BasicNetwork(new HurlStack());

            queue2=new RequestQueue(cache1,network1);
            queue2.start();
            StringRequest request = new StringRequest(Request.Method.POST, Config.pulic_groups, new Response.Listener<String>() {
                @Override
                public void onResponse(String response4) {
                    loadRes(response4);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> mydata = new HashMap<>();
                    // no need to pass imei but as security i passed it
                    mydata.put("imei",myIMEI);

                    return mydata;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            queue2.add(request);
            return null;
        }
    }

    private void loadRes(String response4) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response4);
            String Status=jsonObject.getString("status");
            if(Status.equals("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                    String sr = jsonObject1.getString("sr");
                    String users_count = jsonObject1.getString("users_count");
                    String group_name = jsonObject1.getString("group_name");
                    String group_desc = jsonObject1.getString("group_desc");
                    String user_imei = jsonObject1.getString("user_imei");
                    String user_name = jsonObject1.getString("user_name");
                    String date = jsonObject1.getString("date");
                    String name = jsonObject1.getString("name");
                    String grp_checksum = jsonObject1.getString("grp_checksum");
                    String user_pic = jsonObject1.getString("user_pic");


                    PUBG pubg2 = new PUBG(sr,users_count,group_name,group_desc,user_imei,user_name,date,name,grp_checksum,user_pic);
                    pubg.add(pubg2);

                    PubgRec pubgRec = new PubgRec(getContext(), pubg);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    recPublic.setLayoutManager(layoutManager);
                    recPublic.setHasFixedSize(true);
                    recPublic.setAdapter(pubgRec);
                    //suggestFRec.scrollToPosition(5); //use to focus the item with index
                    //rSuugested.notifyDataSetChanged();

                }
            }
            else
            {
                Toast.makeText(getContext(), "No group Found", Toast.LENGTH_SHORT).show();
                recPublic.setVisibility(View.GONE);
                alerts.setVisibility(View.VISIBLE);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        };
    }
}
