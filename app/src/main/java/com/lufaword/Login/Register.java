package com.lufaword.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lufaword.Configuration.Config;
import com.lufaword.MainActivity;
import com.lufaword.R;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Register extends AppCompatActivity {
    private TextView titleText;
    private LinearLayout registerLay;
    private EditText name;
    private EditText username;
    private EditText email;
    private EditText password;
    private EditText verifyPassword;
    private Button register;
    private TextView loginHere;
    private LinearLayout loginLay;
    private EditText LoginUser;
    private EditText loginPassword;
    private Button loginBtn;
    private TextView regText;
    private TextView subtitleLine;
    RequestQueue queue,queue1,queue2;
ProgressDialog pd;
Animation blink;
    private LinearLayout messageLay;
    private TextView alertMsgTitle;
    private TextView alertMsg;
    private CheckBox checkBox;
    private TextView terms;
    private LinearLayout passresetLay;
    private EditText resetUsername;
    private EditText resetPass;
    private Button resetBtn;
    private TextView resetLoginText;
    private TextView resetText2;
    public static final String REQUEST_TAG = "VolleyBlockingRequestActivity";












    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        titleText = (TextView) findViewById(R.id.title_text);
        registerLay = (LinearLayout) findViewById(R.id.register_lay);
        name = (EditText) findViewById(R.id.name);
        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        verifyPassword = (EditText) findViewById(R.id.verify_password);
        register = (Button) findViewById(R.id.register);
        loginHere = (TextView) findViewById(R.id.login_here);
        loginLay = (LinearLayout) findViewById(R.id.login_lay);
        LoginUser = (EditText) findViewById(R.id.Login_user);
        loginPassword = (EditText) findViewById(R.id.login_password);
        loginBtn = (Button) findViewById(R.id.login_btn);
        regText = (TextView) findViewById(R.id.reg_text);
        subtitleLine = (TextView) findViewById(R.id.subtitle_line);
        messageLay = (LinearLayout) findViewById(R.id.message_lay);
        alertMsgTitle = (TextView) findViewById(R.id.alert_msg_title);
        alertMsg = (TextView) findViewById(R.id.alert_msg);
        checkBox = (CheckBox) findViewById(R.id.check_box);
        terms = (TextView) findViewById(R.id.terms);
        passresetLay = (LinearLayout) findViewById(R.id.passreset_lay);
        resetUsername = (EditText) findViewById(R.id.reset_username);
        resetPass = (EditText) findViewById(R.id.reset_pass);
        resetBtn = (Button) findViewById(R.id.reset_btn);
        resetLoginText = (TextView) findViewById(R.id.reset_login_text);
        resetText2 = (TextView) findViewById(R.id.reset_text2);


        // initilize animation
        blink= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);

        // initilize progress dialogue box
        pd=new ProgressDialog(this);
        pd.setTitle("Processing");
        pd.setMessage("Please wait...");
        pd.setCancelable(true);

        // click on term and conditions text for term and conditions
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Register.this,Terms.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                finish();
            }
        });

        // send on login screen from the password reset
        resetLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleText.setText("Login Here");
                registerLay.setVisibility(View.GONE);
                passresetLay.setVisibility(View.GONE);
                loginLay.setVisibility(View.VISIBLE);
            }
        });

        // click on password reset btn
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reset_username=resetUsername.getText().toString();
                String reset_pass=resetPass.getText().toString();

                if(reset_username.length()<6 || reset_username.length()>40 || reset_username.contains("Fuck You") || reset_username.contains("Fuck") || reset_username.contains("fuck") || reset_username.contains("Fucking"))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid old password",Snackbar.LENGTH_SHORT).show();
                    resetUsername.requestFocus();
                }
                else if(reset_pass.length()<6 || reset_pass.length()>30)
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Require password between 6-30",Snackbar.LENGTH_SHORT).show();
                    resetUsername.requestFocus();
                }
                else
                {
                    resetMyPass(reset_username,reset_pass);
                }
            }
        });
        // click on reset Text
        resetText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleText.setText("Password Reset");
                passresetLay.setVisibility(View.VISIBLE);
                messageLay.setVisibility(View.GONE);
                loginLay.setVisibility(View.GONE);
                registerLay.setVisibility(View.GONE);
            }
        });
        //visible registration layout
        // check wheather logon and registration from presistence memory
        if(GetPresistenceData.getMyLogon(getApplicationContext())=="0")
        {
            titleText.setText("Login");
            loginLay.setVisibility(View.VISIBLE);
        }
        else
        {
            titleText.setText("Registration");
            registerLay.setVisibility(View.VISIBLE);
        }

        // clcik on registration text and open registration layout
        regText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleText.setText("Registration");
                registerLay.setVisibility(View.VISIBLE);
                loginLay.setVisibility(View.GONE);
            }
        });
        // login layout visible on login button click
        loginHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleText.setText("Login Here");
                registerLay.setVisibility(View.GONE);
                loginLay.setVisibility(View.VISIBLE);
            }
        });

        // register user
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Name=name.getText().toString();
                String Username=username.getText().toString();
                String Email=email.getText().toString();
                String Password=password.getText().toString();
                String Verify_Password=verifyPassword.getText().toString();

                if(Name.length()<3 || Name.length()>60 || Name.contains("1") || Name.contains("2") || Name.contains("3") || Name.contains("4") || Name.contains("5") || Name.contains("6") || Name.contains("7") || Name.contains("8") || Name.contains("9") || Name.contains("0") || Username.contains("Fuck You") || Username.contains("Fuck") || Username.contains("fuck") || Username.contains("Fucking") || Username.contains("Fucking"))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid Name",Snackbar.LENGTH_SHORT).show();
                    name.requestFocus();
                }
                else if(Username.length()<3 || Username.length()>40 || Username.contains("Fuck You") || Username.contains("Fuck") || Username.contains("fuck") || Username.contains("Fucking"))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid Username",Snackbar.LENGTH_SHORT).show();
                    username.requestFocus();
                }
                else if(Email.length()<5 || Email.length()>80 || !Email.contains("@"))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid Email id",Snackbar.LENGTH_SHORT).show();
                    email.requestFocus();
                }
                else if(Password.length()<6 || Password.length()>30)
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Minimum 6 digits password required",Snackbar.LENGTH_SHORT).show();
                    password.requestFocus();
                }
                else if(!Verify_Password.equals(Password))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Password not matched",Snackbar.LENGTH_SHORT).show();
                    verifyPassword.requestFocus();
                }
                else
                {
                    if(checkBox.isChecked())
                    {
                        pd.show();
                        saveRegistration(Name,Username,Email,Password);
                    }
                    else
                    {
                        Snackbar.make(getWindow().getDecorView().getRootView(),"Please accept term & Conditions",Snackbar.LENGTH_SHORT).show();

                    }

                }
            }
        });

        // Login user by click on login button
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Loginuser=LoginUser.getText().toString();
                String LoginPassword=loginPassword.getText().toString();

                if(Loginuser.length()<3 || Loginuser.length()>40 || Loginuser.contains("Fuck You") || Loginuser.contains("Fuck") || Loginuser.contains("fuck") || Loginuser.contains("Fucking"))
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid Username",Snackbar.LENGTH_SHORT).show();
                    LoginUser.requestFocus();
                }
                else if(LoginPassword.length()<6 || LoginPassword.length()>30)
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Invalid Password",Snackbar.LENGTH_SHORT).show();
                    loginPassword.requestFocus();
                }
                else
                {
                    pd.show();
                    checkLogin(Loginuser,LoginPassword);
                }
            }
        });


    } //onCreate closer

    private void resetMyPass(final String reset_username, final String reset_pass) {
        queue2 = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
        StringRequest request = new StringRequest(Request.Method.POST, Config.resetPass, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData3(response1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("old_pass",reset_username);
                mydata.put("username",GetPresistenceData.getUsername(getApplicationContext()));
                mydata.put("password",reset_pass);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue2.add(request);
    }

    private void parseData3(String response1) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            switch (Status) {
                case "0":
                    pd.dismiss();
                    passresetLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.VISIBLE);
                    alertMsgTitle.setText("Alert Message");
                    alertMsg.setText("Username not found");
                    alertMsg.setAnimation(blink);
                    closeBlink("P");
                    break;
                case "1": // success
                    Toast.makeText(this, "Password reset successfully", Toast.LENGTH_SHORT).show();
                    passresetLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.VISIBLE);
                    titleText.setText("Login");
                        pd.dismiss();
                    break;
                case "2": // server failed

                    pd.dismiss();
                    passresetLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.VISIBLE);
                    alertMsgTitle.setText("Alert Message");
                    alertMsg.setText("Server not responding");
                    alertMsg.setAnimation(blink);
                    closeBlink("P");
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkLogin(final String loginuser, final String loginPassword) {
        queue1 = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
        StringRequest request = new StringRequest(Request.Method.POST, Config.checkLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData2(response1,loginPassword,loginuser);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("username",loginuser);
                mydata.put("password",loginPassword);

                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue1.add(request);
    }

    private void parseData2(String response1, String loginPassword, String loginuser) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            switch (Status) {
                case "0":
                    pd.dismiss();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Incorrect username", Snackbar.LENGTH_SHORT).show();
                    registerLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.VISIBLE);
                    alertMsgTitle.setText("Alert Message");
                    alertMsg.setText("Incorrect username");
                    alertMsg.setAnimation(blink);
                    closeBlink("L");
                    break;
                case "1": // success

                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                        String sr = jsonObject1.getString("sr");
                        String reg_name = jsonObject1.getString("reg_name");
                        String reg_username = jsonObject1.getString("reg_username");
                        String reg_email = jsonObject1.getString("reg_email");
                        String reg_pass = jsonObject1.getString("reg_pass");
                        String profile_pic = jsonObject1.getString("profile_pic");

                        Snackbar.make(getWindow().getDecorView().getRootView(), "Register successfully", Snackbar.LENGTH_SHORT).show();
                        PresistenceData.saveProfile(getApplicationContext(),sr,reg_name,reg_username,reg_email,profile_pic,reg_pass);
                        PresistenceData.checkLogon(getApplicationContext(), "1");
                        PresistenceData.checkRegistration(getApplicationContext(), "1");
                        PresistenceData.saveIMEI(getApplicationContext(),sr);
                        pd.dismiss();

                        // send the user to mainActivity
                        Intent intent = new Intent(Register.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void closeBlink(final String value) {
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(value.equals("L")) {
                    messageLay.setVisibility(View.GONE);
                    registerLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.VISIBLE);
                }
                else if(value.equals("P"))
                {
                    messageLay.setVisibility(View.GONE);
                    registerLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.GONE);
                    passresetLay.setVisibility(View.VISIBLE);
                }
                else
                {
                    loginLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.GONE);
                    registerLay.setVisibility(View.VISIBLE);

                }
            }
        },3000);
    }

    private void saveRegistration(final String name, final String username, final String email, final String password) {
        queue = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
        StringRequest request = new StringRequest(Request.Method.POST, Config.saveRegistration, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                parseData1(response1,name,username,email,password);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> mydata = new HashMap<>();
                mydata.put("name",name);
                mydata.put("username",username);
                mydata.put("email",email);
                mydata.put("pass",password);
                return mydata;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void parseData1(String response1,String name,String username,String email,String password) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            String imei = jsonObject.getString("imei");
            String dp = jsonObject.getString("dp");
            switch (Status) {
                case "0":
                    pd.dismiss();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Server response failed", Snackbar.LENGTH_SHORT).show();
                    registerLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.VISIBLE);
                    alertMsgTitle.setText("Alert Message");
                    alertMsg.setText("Server Not Responding");
                    alertMsg.setAnimation(blink);
                    closeBlink("R");
                    break;
                case "1": // success
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Register successfully", Snackbar.LENGTH_SHORT).show();
                    PresistenceData.saveProfile(getApplicationContext(),imei,name,username,email,dp,password);
                    PresistenceData.checkLogon(getApplicationContext(),"1");
                    PresistenceData.checkRegistration(getApplicationContext(),"1");
                    PresistenceData.saveIMEI(getApplicationContext(),imei);
                    pd.dismiss();

                    // send the user to mainActivity
                    Intent intent=new Intent(Register.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter,R.anim.exit);
                    finish();

                    break;
                case "2":
                    // already exist
                    pd.dismiss();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "User already exist", Snackbar.LENGTH_SHORT).show();
                    registerLay.setVisibility(View.GONE);
                    loginLay.setVisibility(View.GONE);
                    messageLay.setVisibility(View.VISIBLE);
                    alertMsgTitle.setText("Alert Message");
                    alertMsg.setText("User Already Registered");
                    alertMsg.setAnimation(blink);
                    closeBlink("R");
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // close all connection on stop method
    @Override
    protected void onStop() {
        super.onStop();
        if(queue !=null)
        {
            queue.cancelAll(REQUEST_TAG);
        }
    }


}
