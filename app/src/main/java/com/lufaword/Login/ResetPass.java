package com.lufaword.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lufaword.Configuration.Config;
import com.lufaword.MainActivity;
import com.lufaword.Profile.MainProfile;
import com.lufaword.R;
import com.lufaword.Splash;
import com.lufaword.Utils.GetPresistenceData;
import com.lufaword.Utils.PresistenceData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ResetPass extends AppCompatActivity {
    private ImageView GoBack;
    private TextView headerTitle;
    private LinearLayout passresetLay;
    private EditText oldPass;
    private EditText newPass;
    private Button resetBtn;
    String oldpasss,npass,uName;
    RequestQueue queue2;
    private EditText userName;
    ProgressDialog pd;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_pass);
        GoBack = (ImageView) findViewById(R.id.GoBack);
        headerTitle = (TextView) findViewById(R.id.header_title);
        passresetLay = (LinearLayout) findViewById(R.id.passreset_lay);
        oldPass = (EditText) findViewById(R.id.old_pass);
        newPass = (EditText) findViewById(R.id.new_pass);
        resetBtn = (Button) findViewById(R.id.reset_btn);
        userName = (EditText) findViewById(R.id.userName);
        pd=new ProgressDialog(this);
        pd.setTitle("Reseting password");
        pd.setMessage("Please wait...");

        // set constraints
        headerTitle.setText("RESET PASSWORD");

        // click on Goback button
        GoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ResetPass.this, MainProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        /// click on password reset button
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            oldpasss=oldPass.getText().toString();
            npass=newPass.getText().toString();
            uName=userName.getText().toString();

            if(uName.length()<4 || uName.length()>40)
            {
                Toast.makeText(ResetPass.this,"Invalid username", Toast.LENGTH_SHORT).show();
                userName.requestFocus();
            }
            else if(oldpasss.length()<2 || oldpasss.length()>40)
            {
                Toast.makeText(ResetPass.this,"Invalid old pass", Toast.LENGTH_SHORT).show();
                oldPass.requestFocus();
            }
            else if(npass.length()<6 || npass.length()>40)
            {
                Toast.makeText(ResetPass.this,"Require 6 digit password minimum", Toast.LENGTH_SHORT).show();
                newPass.requestFocus();
            }
            else
            {
                pd.show();
                queue2 = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
                StringRequest request = new StringRequest(Request.Method.POST, Config.resetPass, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        parseData3(response1);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    public Map<String, String> getParams() {
                        Map<String, String> mydata = new HashMap<>();
                        mydata.put("old_pass",oldpasss);
                        mydata.put("username", uName);
                        mydata.put("password",npass);

                        return mydata;
                    }
                };
                int socketTimeout = 30000;
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                request.setRetryPolicy(policy);
                queue2.add(request);
            }

            }
        });


    }// onCreate closer

    private void parseData3(String response1) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            switch (Status) {
                case "0":
                    pd.dismiss();
                    Toast.makeText(this,"Invalid username and password", Toast.LENGTH_SHORT).show();
                    oldPass.setText("");
                    newPass.setText("");
                    userName.setText("");
                    break;
                case "1": // success
                    pd.dismiss();
                    Toast.makeText(this,"Password reset successfully", Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(ResetPass.this, MainProfile.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
