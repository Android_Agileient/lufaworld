package com.lufaword.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lufaword.Configuration.Config;
import com.lufaword.MainActivity;
import com.lufaword.Profile.MainProfile;
import com.lufaword.R;
import com.lufaword.Splash;
import com.lufaword.Utils.PresistenceData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ResetUsername extends AppCompatActivity {
    private ImageView GoBack;
    private TextView headerTitle;
    private LinearLayout passresetLay;
    private EditText username;
    private EditText resetUsername;
    private EditText verifyPass;
    private Button resetBtn;
    String userNaMe,resetUname,vPass;
    RequestQueue queue;
    ProgressDialog pd;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_username);
        GoBack = (ImageView) findViewById(R.id.GoBack);
        headerTitle = (TextView) findViewById(R.id.header_title);
        passresetLay = (LinearLayout) findViewById(R.id.passreset_lay);
        username = (EditText) findViewById(R.id.username);
        resetUsername = (EditText) findViewById(R.id.reset_username);
        verifyPass = (EditText) findViewById(R.id.verify_pass);
        resetBtn = (Button) findViewById(R.id.reset_btn);
        pd=new ProgressDialog(this);
        pd.setTitle("Reseting password");
        pd.setMessage("Please wait...");

        // setConstraints
        headerTitle.setText("RESET USERNAME");

        // click on goback button
        GoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ResetUsername.this, MainProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        // reset button on click listner in order to reset username
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNaMe=username.getText().toString();
                resetUname=resetUsername.getText().toString();
                vPass=verifyPass.getText().toString();


                if(userNaMe.length()<2 || userNaMe.length()>40)
                {
                    Toast.makeText(ResetUsername.this, "Invalid username", Toast.LENGTH_SHORT).show();
                    username.requestFocus();
                }
                else if(resetUname.length()<2 || resetUname.length()>40)
                {
                    Toast.makeText(ResetUsername.this, "Invalid username", Toast.LENGTH_SHORT).show();
                    resetUsername.requestFocus();
                }
                else
                {
                    pd.show();
                    queue = Volley.newRequestQueue((Objects.requireNonNull(getApplicationContext())));
                    StringRequest request = new StringRequest(Request.Method.POST, Config.update_username, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response1) {
                            parseData1(response1);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                        @Override
                        public Map<String, String> getParams() {
                            Map<String, String> mydata = new HashMap<>();
                            mydata.put("new_username",resetUname);
                            mydata.put("old_username",userNaMe);
                            mydata.put("password",vPass);
                            return mydata;
                        }
                    };
                    int socketTimeout = 30000;
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    request.setRetryPolicy(policy);
                    queue.add(request);
                }
            }
        });



    } //onCreate closer
    private void parseData1(String response1){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response1);
            String Status = jsonObject.getString("status");
            switch (Status) {
                case "0":
                    pd.dismiss();
                    Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                    username.setText("");
                    resetUsername.setText("");
                    verifyPass.setText("");
                    break;
                case "1": // success
                    pd.dismiss();
                    Toast.makeText(this, "Username updated successfully", Toast.LENGTH_SHORT).show();
                    PresistenceData.saveIMEI(getApplicationContext(),null);
                    PresistenceData.checkRegistration(getApplicationContext(),"0");
                    PresistenceData.checkLogon(getApplicationContext(),"0");
                    PresistenceData.saveProfile(getApplicationContext(),null,null,null,null,null,null);

                    Intent intent=new Intent(ResetUsername.this, Splash.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
